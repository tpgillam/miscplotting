#!/usr/bin/env python 
import sys
if '-b' not in sys.argv:
  sys.argv.append('-b')
  import ROOT
  sys.argv = sys.argv[:-1]

def main():
  #treePath = '/r02/atlas/gillam/histFitterRPV/p1328_20130221_OS2b/'
  #treePath = '/r02/atlas/gillam/histFitterRPV/20130302RPV-OS2b_1/'
  #treePath = '/r02/atlas/gillam/histFitterRPV/20130604RPV-treeProd19/'
  treePath = '/r02/atlas/gillam/histFitterRPV/20130812RPV-treeProd29_OS2b/'
  outputDir = 'plots_20130812RPV-treeProd29_OS2b-noBWeights'

  import ROOT
  ROOT.gROOT.SetBatch(True)
  ROOT.gROOT.LoadMacro("AtlasStyle.C")
  ROOT.gROOT.LoadMacro("AtlasLabels.C")
  ROOT.SetAtlasStyle()

  import os.path
  backgroundsFile = ROOT.TFile(os.path.join(treePath, 'backgrounds.root'))
  fakeEstimateFile = ROOT.TFile(os.path.join(treePath, 'fakeEstimate.root'))

  from comparator import MCBackground, MCBackgroundList
  backgroundDict = {}
  for systematicPostfix in ['nom', 'JER', 'JESUP', 'JESDOWN', 'RESOST', 'SCALESTUP', 'SCALESTDOWN']:
    mcBackgrounds = MCBackgroundList()
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBoson_'+systematicPostfix), name='diBoson', label='Sherpa diBoson', color=ROOT.kRed+3))
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('topV_'+systematicPostfix), name='topV', label='MadGraph ttbar+V', color=ROOT.kRed-2))
    mcBackgrounds.append(MCBackground(fakeEstimateFile.Get('fakes_nom'), name='fakes', label='Fake leptons', color=ROOT.kSpring-7, isDataDriven=True))
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('singleTop_'+systematicPostfix), name='singleTop', label='McAtNlo/AcerMC Single top', color=ROOT.kCyan-2))
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('topPowheg_'+systematicPostfix), name='top', label='Powheg ttbar', color=ROOT.kYellow-3))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('top_'+systematicPostfix), name='top', label='MCAtNlo ttbar', color=ROOT.kYellow-3))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarBB_'+systematicPostfix), name='ttbb', label='Alpgen ttbb', color=ROOT.kSpring-3))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarCC_'+systematicPostfix), name='ttcc', label='Alpgen ttcc', color=ROOT.kPink+10))
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('zJetsMassiveB_'+systematicPostfix), name='zJetsMassiveB', label='Sherpa Z+jets (massive B)', color=ROOT.kViolet-6))
    backgroundDict[systematicPostfix] = mcBackgrounds

  egammaPath = os.path.join(treePath, 'egamma.root')
  muonPath = os.path.join(treePath, 'muon.root')
  jettauetmissPath = os.path.join(treePath, 'jettauetmiss.root')

  dataChain = ROOT.TChain('data')
  dataChain.AddFile(egammaPath)
  dataChain.AddFile(muonPath)
  dataChain.AddFile(jettauetmissPath)

  from comparator import ComparisonInputs
  comparisonInputs = ComparisonInputs(dataChain, backgroundDict['nom'])
  comparisonInputs.addTreeSystematic('JER', backgroundDict['JER'])
  comparisonInputs.addTreeSystematic('JES', backgroundDict['JESUP'], backgroundDict['JESDOWN'])
  comparisonInputs.addTreeSystematic('RESOST', backgroundDict['RESOST'])
  comparisonInputs.addTreeSystematic('SCALEST', backgroundDict['SCALESTUP'], backgroundDict['SCALESTDOWN'])
  #nominalWeights = ['mcWgt', 'pileupWgt', 'trigWgt', 'eGammaWgt', 'muonWgt', 'lumiScaling' ,'bTagWgt']
  nominalWeights = ['mcWgt', 'pileupWgt', 'trigWgt', 'lumiScaling']
  for weight in nominalWeights:
    comparisonInputs.addWeight(weight)
  comparisonInputs.addWeightWithSystematic('BJET', 'bTagWgt', 'bTagWgt_BJETUP', 'bTagWgt_BJETDOWN')
  comparisonInputs.addWeightWithSystematic('MISTAG', 'bTagWgt', 'bTagWgt_MISTAGUP', 'bTagWgt_MISTAGDOWN')
  comparisonInputs.addWeightWithSystematic('EGAMMA', 'eGammaWgt', 'eGammaWgt_UP', 'eGammaWgt_DOWN')
  comparisonInputs.addWeightWithSystematic('MUON', 'muonWgt', 'muonWgt_UP', 'muonWgt_DOWN')
  comparisonInputs.addWeightWithSystematic('CHARGEFLIP', 'chargeFlipWgt', 'chargeFlipWgt_UP', mcBackgroundNamesToUse=['chargeFlip', 'fakes'])
  useFullySeparatedSysts = True
  if useFullySeparatedSysts:
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr0', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin0_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr1', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin1_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr2', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin2_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr3', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin3_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr4', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin4_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr5', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin5_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr6', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin6_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr7', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin7_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr8', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin8_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr9', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin9_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr10', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin10_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr11', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin11_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr12', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin12_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr13', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin13_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr14', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin14_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr15', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin15_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr16', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin16_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr17', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin17_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElCorr', 'fakeLeptWgt', 'fakeLeptWgt_ElCorr_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr0', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin0_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr1', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin1_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr2', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin2_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr3', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin3_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuCorr', 'fakeLeptWgt', 'fakeLeptWgt_MuCorr_UP', mcBackgroundNamesToUse='fakes')
  else:
    comparisonInputs.addWeightWithSystematic('FAKEEL', 'fakeLeptWgt', 'fakeLeptWgt_El_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMU', 'fakeLeptWgt', 'fakeLeptWgt_Mu_UP', mcBackgroundNamesToUse='fakes')

  from comparator import Comparison
  comparisons = []
  for exclusive in [True, False]:
    #for nBJets in [2,3]:
    for nBJets in [3]:
      for lepChan in [None, 0, 1, 2]:
        name = '-'+str(nBJets)+'b'
        if exclusive:
          name += '-excl'
        if lepChan is not None:
          if lepChan == 0:
            name += '-ee'
          elif lepChan == 1:
            name += '-em'
          elif lepChan == 2:
            name += '-mm'

        if exclusive:
          weight = '(nBJets20=={0})'.format(nBJets)
        else:
          weight = '(nBJets20>={0})'.format(nBJets)
        if lepChan is not None:
          weight += ' * (chanDilep=={0})'.format(lepChan)
        comparisons.append(Comparison('lep1Pt/1000', ROOT.TH1D('lep1Pt'+name, ';Leading lepton p_{T} [GeV];Events / 10 GeV', 40, 0, 400), weight))
        comparisons.append(Comparison('mll/1000', ROOT.TH1D('mll'+name, ';m_{ll} [GeV];Events / 60 GeV', 10, 0, 600), weight))
        comparisons.append(Comparison('met/1000', ROOT.TH1D('met'+name, ';E^{T}_{miss} [GeV];Events / 50 GeV', 10, 0, 500), weight))
        comparisons.append(Comparison('meff/1000', ROOT.TH1D('meff'+name, ';m_{eff} [GeV];Events / 100 GeV', 15, 0, 1500), weight))
        comparisons.append(Comparison('nJets20', ROOT.TH1D('nJets20'+name, ';nJets > 20 GeV;Events', 12, 2, 14), weight))
        comparisons.append(Comparison('nBJets20', ROOT.TH1D('nBJets20'+name, ';nBJets > 20 GeV;Events', 7, 2, 9), weight))
        comparisons.append(Comparison('bJet1Pt/1000', ROOT.TH1D('bJet1Pt'+name, ';1st hardest b-jet p_{T} [GeV];Events / 50 GeV', 10, 0, 500), weight))
        comparisons.append(Comparison('bJet1Pt/1000', ROOT.TH1D('bJet1Pt_10'+name, ';1st hardest b-jet p_{T} [GeV];Events / 10 GeV', 50, 0, 500), weight))
        comparisons.append(Comparison('bJet1Pt/1000', ROOT.TH1D('bJet1Pt_5'+name, ';1st hardest b-jet p_{T} [GeV];Events / 5 GeV', 100, 0, 500), weight))
        comparisons.append(Comparison('bJet2Pt/1000', ROOT.TH1D('bJet2Pt'+name, ';2nd hardest b-jet p_{T} [GeV];Events / 50 GeV', 10, 0, 500), weight))
        if nBJets == 3:
          comparisons.append(Comparison('bJet3Pt/1000', ROOT.TH1D('bJet3Pt'+name, ';3rd hardest b-jet p_{T} [GeV];Events / 40 GeV', 10, 0, 400), weight))

  from comparator import makeComparisons
  makeComparisons(outputDir, comparisons, comparisonInputs, None, 20661.0/20694.9)

  backgroundsFile.Close()
  fakeEstimateFile.Close()


if __name__ == '__main__':
  main()

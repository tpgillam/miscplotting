#!/usr/bin/env python
from ROOT import TCanvas, TPad, TH1D, kGray
h = TH1D('moo','moo',3,0,3)
h2 = TH1D('moo2','moo2',4,0,6)

c = TCanvas()
p = TPad('p','p',0,0.3,1,1)
p2 = TPad('p2','p2',0,0,1,0.3)
p.Draw()
p2.Draw()

p.SetFillColor(kGray)
p.cd()
h.Draw()
p.Modified()


p2.SetFillColor(kGray)
p2.cd()
h2.Draw()
p2.Draw()


from time import sleep
sleep(1)


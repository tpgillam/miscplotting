#!/usr/bin/env python 
import sys
if '-b' not in sys.argv:
  sys.argv.append('-b')
  import ROOT
  sys.argv = sys.argv[:-1]

from gillam.rootcolours import solarized

from comparator import ComparisonOptions
globalOptions = ComparisonOptions()
#globalOptions.backgroundColor = solarized.base3

def main():
  shouldMakeTables = False

  #treePath = '/r02/atlas/gillam/histFitterRPV/20130929NoTRT-take1'
  #outputDir = 'plotsSS_20130029NoTRT-take1'
  treePath = '/r02/atlas/gillam/histFitterRPV/20130812RPV-treeProd48'

  #outputDir = 'plotsSS_20130812RPV-treeProd45'
  outputDir = 'plotsSS_20130812RPV-treeProd48-flowergridRegions'

  mcFakes = False
  if mcFakes:
    outputDir += '-mcFakes'

  import ROOT
  ROOT.gROOT.SetBatch(True)
  ROOT.gROOT.LoadMacro('AtlasStyle.C')
  ROOT.gROOT.LoadMacro('AtlasLabels.C')
  ROOT.SetAtlasStyle()
  # Allow us to set histogram background colours..!
  ROOT.gROOT.ForceStyle(0)

  import os.path
  if mcFakes:
    backgroundsFile = ROOT.TFile(os.path.join(treePath, 'backgroundsNoFR.root'))
  else:
    backgroundsFile = ROOT.TFile(os.path.join(treePath, 'backgrounds.root'))
  fakeEstimateFile = ROOT.TFile(os.path.join(treePath, 'fakeEstimate.root'))
  chargeFlipFile = ROOT.TFile(os.path.join(treePath, 'chargeFlip.root'))
  #fakeEstimateFile = ROOT.TFile(os.path.join(treePath, 'fakeEstimate_skimmed.root'))
  #chargeFlipFile = ROOT.TFile(os.path.join(treePath, 'chargeFlip_skimmed.root'))

  from comparator import MCBackground, MCBackgroundList
  backgroundDict = {}
  for systematicPostfix in ['nom', 'JER', 'JESUP', 'JESDOWN', 'RESOST', 'SCALESTUP', 'SCALESTDOWN']:
    mcBackgrounds = MCBackgroundList()
    
    higgsChain = ROOT.TChain()
    if mcFakes:
      backgroundsPath = os.path.join(treePath, 'backgroundsNoFR.root')
    else:
      backgroundsPath = os.path.join(treePath, 'backgrounds.root')
    higgsChain.AddFile(backgroundsPath, ROOT.TChain.kBigNumber, 'wHiggs_'+systematicPostfix)
    higgsChain.AddFile(backgroundsPath, ROOT.TChain.kBigNumber, 'zHiggs_'+systematicPostfix)
    higgsChain.AddFile(backgroundsPath, ROOT.TChain.kBigNumber, 'ttbarHiggs_'+systematicPostfix)
    mcBackgrounds.append(MCBackground(higgsChain, name='higgs', label='Pythia8 Higgs', color=solarized.cyan))

    if shouldMakeTables:
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarHiggs_'+systematicPostfix), name='tthiggs', label='Pythia8 ttbar+H', color=solarized.cyan))
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarHiggs_'+systematicPostfix), name='ttH_WW_2SSLep', label='Pythia8 ttbar+H', color=solarized.cyan, extraCut='mcChanNum == 161990'))
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarHiggs_'+systematicPostfix), name='ttH_WWinc', label='Pythia8 ttbar+H', color=solarized.cyan, extraCut='mcChanNum == 161305'))
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarHiggs_'+systematicPostfix), name='ttH_ZZinc', label='Pythia8 ttbar+H', color=solarized.cyan, extraCut='mcChanNum == 169072'))
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarHiggs_'+systematicPostfix), name='ttH_tautaull', label='Pythia8 ttbar+H', color=solarized.cyan, extraCut='mcChanNum == 161708'))
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarHiggs_'+systematicPostfix), name='ttH_tautaulh', label='Pythia8 ttbar+H', color=solarized.cyan, extraCut='mcChanNum == 161719'))


    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('wGamma_'+systematicPostfix), name='wGamma', label='Alpgen WGamma', color=solarized.red))
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('topV_'+systematicPostfix), name='topV', label='MadGraph ttbar+V', color=solarized.magenta))

    #diBosonMCBChain = ROOT.TChain()
    #diBosonMCBChain .AddFile(backgroundsPath, ROOT.TChain.kBigNumber, 'diBosonSherpaMassiveCB_'+systematicPostfix)
    #diBosonMCBChain .AddFile(backgroundsPath, ROOT.TChain.kBigNumber, 'triBoson_'+systematicPostfix)
    #mcBackgrounds.append(MCBackground(diBosonMCBChain, name='diBoson', label='diBoson (MassiveCB) + triBoson', color=solarized.blue))

    diBosonAllSherpaChain = ROOT.TChain()
    diBosonAllSherpaChain.AddFile(backgroundsPath, ROOT.TChain.kBigNumber, 'diBosonOther_'+systematicPostfix)
    diBosonAllSherpaChain.AddFile(backgroundsPath, ROOT.TChain.kBigNumber, 'diBosonSherpaZZ_'+systematicPostfix)
    mcBackgrounds.append(MCBackground(diBosonAllSherpaChain, name='diBoson', label='diBoson + triBoson', color=solarized.blue))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBoson_'+systematicPostfix), name='diBoson', label='diBoson + triBoson', color=solarized.blue))

    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonOther_'+systematicPostfix), name='diBosonOther', label='Sherpa diBoson WW,WZ + MadGraph triBoson', color=solarized.blue))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonWW_'+systematicPostfix), name='diBosonWW', label='Sherpa diBoson WW', color=ROOT.kMagenta+3))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonSS_'+systematicPostfix), name='diBosonSS', label='Sherpa diBoson WW (SS)', color=ROOT.kMagenta+2))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonWZ_'+systematicPostfix), name='diBosonWZ', label='Sherpa diBoson WZ', color=ROOT.kMagenta+1))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonSherpaZZ_'+systematicPostfix), name='diBosonSherpaZZ', label='Sherpa diBoson ZZ', color=ROOT.kBlue))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('triBoson_'+systematicPostfix), name='triBoson', label='MadGraph triBoson', color=ROOT.kMagenta))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonPowhegWW_'+systematicPostfix), name='diBosonWW', label='Powheg diBoson WW', color=ROOT.kMagenta+3))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonPowhegWZ_'+systematicPostfix), name='diBosonWZ', label='Powheg diBoson WZ', color=ROOT.kMagenta+1))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBosonPowhegZZ_'+systematicPostfix), name='diBosonPowhegZZ', label='Powheg diBoson ZZ', color=ROOT.kBlue))

    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('top_'+systematicPostfix), name='top', label='MCAtNlo ttbar', color=ROOT.kYellow-3))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarBB_'+systematicPostfix), name='ttbb', label='Alpgen ttbb', color=ROOT.kSpring-3))
    #mcBackgrounds.append(MCBackground(backgroundsFile.Get('ttbarCC_'+systematicPostfix), name='ttcc', label='Alpgen ttcc', color=ROOT.kPink+10))
    if mcFakes:
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('topPowheg_'+systematicPostfix), name='topPowheg', label='Powheg ttbar', color=solarized.yellow))
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('zJetsMassiveB_'+systematicPostfix), name='zJetsMassiveB', label='Sherpa Z+jets (massive B)', color=solarized.red))
      mcBackgrounds.append(MCBackground(backgroundsFile.Get('singleTop_'+systematicPostfix), name='singleTop', label='singleTop', color=ROOT.kBlue))
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('singleTopZ_'+systematicPostfix), name='singleTopZ', label='singleTopZ', color=ROOT.kAzure+10))

    if not mcFakes:
      fakeEstimate = MCBackground(fakeEstimateFile.Get('fakes_nom'), name='fakes', label='Fake leptons', color=solarized.green, isDataDriven=True)
      mcBackgrounds.append(fakeEstimate)
      mcBackgrounds.append(MCBackground(chargeFlipFile.Get('chargeFlip_nom'), name='chargeFlip', label='Charge flipped leptons', color=solarized.violet, isDataDriven=True))
    backgroundDict[systematicPostfix] = mcBackgrounds

  egammaPath = os.path.join(treePath, 'egamma.root')
  muonPath = os.path.join(treePath, 'muon.root')
  jettauetmissPath = os.path.join(treePath, 'jettauetmiss.root')

  dataChain = ROOT.TChain('data')
  dataChain.AddFile(egammaPath)
  dataChain.AddFile(muonPath)
  dataChain.AddFile(jettauetmissPath)

  from comparator import ComparisonInputs
  comparisonInputs = ComparisonInputs(dataChain, backgroundDict['nom'])
  #nominalWeights = ['mcWgt', 'pileupWgt', 'trigWgt', 'eGammaWgt', 'muonWgt', 'lumiScaling' ,'bTagWgt']
  nominalWeights = ['mcWgt', 'pileupWgt', 'trigWgt', 'lumiScaling']
  for weight in nominalWeights:
    comparisonInputs.addWeight(weight)
  comparisonInputs.addTreeSystematic('JER', backgroundDict['JER'])
  comparisonInputs.addTreeSystematic('JES', backgroundDict['JESUP'], backgroundDict['JESDOWN'])
  comparisonInputs.addTreeSystematic('RESOST', backgroundDict['RESOST'])
  comparisonInputs.addTreeSystematic('SCALEST', backgroundDict['SCALESTUP'], backgroundDict['SCALESTDOWN'])
  comparisonInputs.addWeightWithSystematic('BJET', 'bTagWgt', 'bTagWgt_BJETUP', 'bTagWgt_BJETDOWN')
  comparisonInputs.addWeightWithSystematic('MISTAG', 'bTagWgt', 'bTagWgt_MISTAGUP', 'bTagWgt_MISTAGDOWN')
  comparisonInputs.addWeightWithSystematic('EGAMMA', 'eGammaWgt', 'eGammaWgt_UP', 'eGammaWgt_DOWN')
  comparisonInputs.addWeightWithSystematic('MUON', 'muonWgt', 'muonWgt_UP', 'muonWgt_DOWN')

  comparisonInputs.addWeightWithSystematic('CHARGEFLIP', 'chargeFlipWgt', 'chargeFlipWgt_UP', mcBackgroundNamesToUse=['chargeFlip', 'fakes'])

  useFullySeparatedSysts = False
  if useFullySeparatedSysts:
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr0', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin0_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr1', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin1_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr2', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin2_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr3', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin3_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr4', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin4_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr5', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin5_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr6', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin6_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr7', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin7_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr8', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin8_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr9', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin9_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr10', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin10_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr11', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin11_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr12', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin12_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr13', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin13_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr14', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin14_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr15', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin15_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr16', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin16_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElUncorr17', 'fakeLeptWgt', 'fakeLeptWgt_ElUncorrBin17_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEElCorr', 'fakeLeptWgt', 'fakeLeptWgt_ElCorr_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr0', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin0_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr1', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin1_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr2', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin2_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuUncorr3', 'fakeLeptWgt', 'fakeLeptWgt_MuUncorrBin3_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMuCorr', 'fakeLeptWgt', 'fakeLeptWgt_MuCorr_UP', mcBackgroundNamesToUse='fakes')
  else:
    comparisonInputs.addWeightWithSystematic('FAKEEL', 'fakeLeptWgt', 'fakeLeptWgt_El_UP', mcBackgroundNamesToUse='fakes')
    comparisonInputs.addWeightWithSystematic('FAKEMU', 'fakeLeptWgt', 'fakeLeptWgt_Mu_UP', mcBackgroundNamesToUse='fakes')


  comparisons = []

  # appendBasicComparisons(comparisons)
  # appendTwoLeptonComparisons(comparisons)
  # appendThreeLooseLepComparisons(comparisons)
  # appendCombinedThreeLooseLepComparisons(comparisons)
  # appendThreeLepFlavourComparisons(comparisons)
  # appendExtraComparisons(comparisons)
  # appendMllZoomComparison(comparisons)
  # appendFourLepComparisons(comparisons)
  # appendSRComparisons(comparisons)
  # appendCRVVComparisons(comparisons)
  # appendAgreementDebuggingComparisons(comparisons)
  # appendCustomLepPtPlots(comparisons)

  # append3LEWK_VR0noZa(comparisons)
  # append3LEWK_VR0noZb(comparisons)
  # append3LEWK_VR0noZb2b(comparisons)
  # append3LEWK_VR0noZb3b(comparisons)
  # append3LEWK_VR0noZbhighpt(comparisons)
  # append3LEWK_VR0noZb2bhighpt(comparisons)
  # append3LEWK_VR0noZb3bhighpt(comparisons)

  appendSR3Lhigh(comparisons)
  appendSR3LhighAnyJ40(comparisons)
  appendSR3Lhigh1J40(comparisons)
  appendSR3Lhigh2J40(comparisons)
  appendSR3Lhigh3J40(comparisons)


  if shouldMakeTables:
    from comparator import makeTables
    tablesDir = outputDir.replace('plot', 'table')
    tableComparisons = []
    for comparison in comparisons:
      if ('nBJets20' in comparison.name) and ('fineBin' not in comparison.name):
        tableComparisons.append(comparison)
    makeTables(tablesDir, tableComparisons, comparisonInputs, None, 20661.0/20694.9)
  else:
    from comparator import makeComparisons
    makeComparisons(outputDir, comparisons, comparisonInputs, None, 20661.0/20694.9)

  backgroundsFile.Close()
  fakeEstimateFile.Close()
  chargeFlipFile .Close()

def appendBasicComparisons(comparisons):
  for name, weight in channelNameAndWeights():
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)
 
def channelNameAndWeights(basename=None, baseweight=None):
  #for lepChan in [None, 0, 1, 2]:
  for lepChan in [None]:
    if basename is None or basename == '':
      name = ''
    elif lepChan is None:
      name = basename
    else:
      name = basename+'-'
    if lepChan is not None:
      if lepChan == 0:
        name += 'ee'
      elif lepChan == 1:
        name += 'em'
      elif lepChan == 2:
        name += 'mm'

    if baseweight is None or baseweight == '':
      weight = '1'
    else:
      weight = baseweight
    if lepChan is not None:
      weight += ' * (chanDilep=={0})'.format(lepChan)
    yield name, blindWeight(weight)

def blindWeight(weight):
  #return '{0} * (nBJets20 <= 2 && (met <= 150000 || nJets20 < 3))'.format(weight)
  return weight

def appendComparisonsForNameAndWeight(comparisons, name, weight, binMultiplier=1, options=None):
  #weight += ' * (lep2Pt > 20000) * (lep3Pt < 0 || lep3Pt > 20000)'
  if name == '':
    nameWithLeadingHyphen = ''
  else:
    nameWithLeadingHyphen = '-' + name
  appendComparison(comparisons, 'mll'+nameWithLeadingHyphen, weight, 'mll/1000', 'm_{ll}', 'GeV', 0, 600, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'mllSFOS1'+nameWithLeadingHyphen, weight, 'mllSFOS1/1000', 'm_{SFOS1}', 'GeV', 0, 600, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'mllSFOS2'+nameWithLeadingHyphen, weight, 'mllSFOS2/1000', 'm_{SFOS2}', 'GeV', 0, 600, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'met'+nameWithLeadingHyphen, weight, 'met/1000', 'E^{T}_{miss}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'meff'+nameWithLeadingHyphen, weight, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, int(round(15*binMultiplier)), options)
  appendComparison(comparisons, 'ht'+nameWithLeadingHyphen, weight, 'ht/1000', 'H_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'mt'+nameWithLeadingHyphen, weight, 'mt/1000', 'm_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'nJets20'+nameWithLeadingHyphen, weight, 'nJets20', 'nJets > 20 GeV', None, 0, 12, 12, options)
  appendComparison(comparisons, 'nJets40'+nameWithLeadingHyphen, weight, 'nJets40', 'nJets > 40 GeV', None, 0, 12, 12, options)
  appendComparison(comparisons, 'nBJets20'+nameWithLeadingHyphen, weight, 'nBJets20', 'nBJets > 20 GeV', None, 0, 12, 12, options)
  appendComparison(comparisons, 'lep1Pt'+nameWithLeadingHyphen, weight, 'lep1Pt/1000', '1st hardest lepton p_{T}', 'GeV', 0, 300, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'lep2Pt'+nameWithLeadingHyphen, weight, 'lep2Pt/1000', '2nd hardest lepton p_{T}', 'GeV', 0, 200, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'lep3Pt'+nameWithLeadingHyphen, weight, 'lep3Pt/1000', '3rd hardest lepton p_{T}', 'GeV', 0, 100, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'lep1Eta'+nameWithLeadingHyphen, weight, 'lep1Eta', '1st hardest lepton \eta', '', -3, 3, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'lep2Eta'+nameWithLeadingHyphen, weight, 'lep2Eta', '2nd hardest lepton \eta', '', -3, 3, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'jet1Pt'+nameWithLeadingHyphen, weight, 'jet1Pt/1000', '1st hardest jet p_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'jet2Pt'+nameWithLeadingHyphen, weight, 'jet2Pt/1000', '2nd hardest jet p_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)

def appendComparison(comparisons, fullName, weight, variable, xLabel, units, xMin, xMax, numBins, options=None):
  if units is None:
    title = ';'+xLabel+';Events'
  elif units == '':
    unitsPerBin = int(round((xMax-xMin)/numBins))
    title = ';'+xLabel+';Events / {0}'.format(unitsPerBin)
  else:
    unitsPerBin = int(round((xMax-xMin)/numBins))
    title = ';'+xLabel+' [{0}];Events / {1} {0}'.format(units, unitsPerBin)
  from comparator import Comparison
  if options is None:
    options = globalOptions
  comparisons.append(Comparison(variable, ROOT.TH1D(fullName, title, numBins, xMin, xMax), weight, options))

def appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight):
  if name == '':
    modifiedName = 'fineBin'
  else:
    modifiedName = 'fineBin-'+name
  appendComparisonsForNameAndWeight(comparisons, modifiedName, weight, binMultiplier=5)

def appendTwoLeptonComparisons(comparisons):
  for name, weight in channelNameAndWeights('2loose', '(nLooseLep == 2)'):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def appendThreeLooseLepComparisons(comparisons):
  for tlConfig in threeLeptonWithTwoTightTLConfigs():
    basename = '3loose' + tlConfig.asString()
    baseweight = '(nLooseLep == 3) * (TLBitmask == {0})'.format(tlConfig.asBitmask())
    for name, weight in channelNameAndWeights(basename, baseweight):
      appendComparisonsForNameAndWeight(comparisons, name, weight)
      appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)
      if tlConfig.asString() == 'TTT':
        vrnozaWeight = weight + ' * (((mllSFOS1 < 81200) || (mllSFOS1 > 101200)) && ((mllSFOS2 < 81200) || (mllSFOS2 > 101200))) * (nBJets25 == 0) * (met > 35000 && met < 50000)'
        vrnozaName = name + '-vrnoza'
        vrnozbWeight = weight + ' * (((mllSFOS1 < 81200) || (mllSFOS1 > 101200)) && ((mllSFOS2 < 81200) || (mllSFOS2 > 101200))) * (nBJets25 > 0) * (met > 50000)'
        vrnozbName = name + '-vrnozb'
        vrzaWeight = weight + ' * (((mllSFOS1 > 81200) && (mllSFOS1 < 101200)) || ((mllSFOS2 > 81200) && (mllSFOS2 < 101200))) * (nBJets25 == 0) * (met > 35000 && met < 50000)'
        vrzaName = name + '-vrza'
        vrzbWeight = weight + ' * (((mllSFOS1 > 81200) && (mllSFOS1 < 101200)) || ((mllSFOS2 > 81200) && (mllSFOS2 < 101200))) * (nBJets25 > 0) * (met > 50000)'
        vrzbName = name + '-vrzb'
        appendComparisonsForNameAndWeight(comparisons, vrnozaName, vrnozaWeight)
        appendComparisonsForNameAndWeight(comparisons, vrnozbName, vrnozbWeight)
        appendComparisonsForNameAndWeight(comparisons, vrzaName, vrzaWeight)
        appendComparisonsForNameAndWeight(comparisons, vrzbName, vrzbWeight)

def appendCombinedThreeLooseLepComparisons(comparisons):
  for name, weight in channelNameAndWeights('3loose', '(nLooseLep == 3)'):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def appendThreeLepFlavourComparisons(comparisons):
  for flavConfig in threeLeptonTLConfigs():
    name = '3Lflav' + flavConfig.asFlavourString()
    weight = '(nLep15 == 3) * (isElBitmask == {0})'.format(flavConfig.asBitmask())
    weight = blindWeight(weight)
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

    name = '3LflavOverview' + flavConfig.asFlavourString()
    histogram = ROOT.TH1D(name, name+';Input TL config;Events', 8, 0, 8)
    addTLLabels(histogram)
    from comparator import Comparison
    #weight += ' * (lep2Pt > 20000) * (lep3Pt < 0 || lep3Pt > 20000)'
    comparisons.append(Comparison('origTLBitmask', histogram, weight, options=globalOptions))
  
  name = '3LflavOverview'
  weight = '(nLep15 >= 3)'
  weight = blindWeight(weight)
  histogram = ROOT.TH1D(name, name+';Flavour config;Events', 9, 0, 9)
  addFlavourLabels(histogram)
  from comparator import Comparison
  #weight += ' * (lep2Pt > 20000) * (lep3Pt < 0 || lep3Pt > 20000)'
  comparisons.append(Comparison('((nLep15 == 3) * isElBitmask + (nLep15 > 3) * 8.5)', histogram, weight, options=globalOptions))

def appendExtraComparisons(comparisons):
  name = 'meff500'
  weight = '(chanDilep == 0) * (meff > 450000) * (meff < 550000)'
  weight = blindWeight(weight)
  appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def appendMllZoomComparison(comparisons):
  name = 'mllZoom'
  weight = '1'
  #weight += ' * (lep2Pt > 20000) * (lep3Pt < 0 || lep3Pt > 20000)'
  weight = blindWeight(weight)
  appendComparison(comparisons, name, weight, 'mll/1000', 'm_{ll}', 'GeV', 70, 110, 40)

def appendFourLepComparisons(comparisons):
  name = '4LflavOverview'
  weight = '(nLep15 >= 4)'
  weight = blindWeight(weight)
  histogram = ROOT.TH1D(name, name+';Flavour config;Events', 17, 0, 17)
  appendFourLeptonFlavourLables(histogram)
  from comparator import Comparison
  comparisons.append(Comparison('((nLep15 == 4) * isElBitmask + (nLep15 > 4) * 16.5)', histogram, weight, options=globalOptions))

def appendSRComparisons(comparisons):
  nameOffshell = 'SR3Lep_offshell'
  nameOnshell = 'SR3Lep_onshell'
  zMassVeto3L = '(mllSFOS1 < 81200 || mllSFOS1 > 101200) * (mllSFOS2 < 81200 || mllSFOS2 > 101200)'
  notSR3B = '!(nBJets20 >= 3 && nJets40 >= 4)'
  basicCutsOffshell = '(nJets40 >= 4) * (met > 50000) * (met < 150000) * (nLep15 > 2)'
  basicCutsOnshell = '(nJets40 >= 4) * (met > 150000) * (nLep15 > 2)'
  weightOffshell = ' * '.join([basicCutsOffshell, zMassVeto3L, notSR3B])
  weightOnshell = ' * '.join([basicCutsOnshell, notSR3B])
  weightOffshell = blindWeight(weightOffshell)
  weightOnshell = blindWeight(weightOnshell)
  from comparator import ComparisonOptions
  options = ComparisonOptions()
  options.blinded = True
  options.useLinearScale = True
  options.backgroundColor = globalOptions.backgroundColor
  appendComparisonsForNameAndWeight(comparisons, nameOffshell, weightOffshell, binMultiplier=0.25, options=options)
  appendComparison(comparisons, nameOffshell, weightOffshell, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, 1, options=options)
  appendComparisonsForNameAndWeight(comparisons, nameOnshell, weightOnshell, binMultiplier=0.25, options=options)
  appendComparison(comparisons, nameOnshell, weightOnshell, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, 1, options=options)

def appendSR3Lhigh(comparisons):
  baseweight = 'nLep15>=3 && nJets40>=4 && met>150000 && !(nLep15>=2 && nJets40>= 5 && nBJets20>=3)'
  for name, weight in channelNameAndWeights('SR3Lhigh', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def appendSR3LhighAnyJ40(comparisons):
  baseweight = 'nLep15>=3 && met>150000 && !(nLep15>=2 && nJets40>= 5 && nBJets20>=3)'
  for name, weight in channelNameAndWeights('SR3LhighAnyJ40', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def appendSR3Lhigh1J40(comparisons):
  baseweight = 'nLep15>=3 && nJets40>=1 && met>150000 && !(nLep15>=2 && nJets40>= 5 && nBJets20>=3)'
  for name, weight in channelNameAndWeights('SR3Lhigh1J40', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)
def appendSR3Lhigh2J40(comparisons):
  baseweight = 'nLep15>=3 && nJets40>=2 && met>150000 && !(nLep15>=2 && nJets40>= 5 && nBJets20>=3)'
  for name, weight in channelNameAndWeights('SR3Lhigh2J40', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)
def appendSR3Lhigh3J40(comparisons):
  baseweight = 'nLep15>=3 && nJets40>=3 && met>150000 && !(nLep15>=2 && nJets40>= 5 && nBJets20>=3)'
  for name, weight in channelNameAndWeights('SR3Lhigh3J40', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def appendCRVVComparisons(comparisons):
  name = 'CRVV'
  weight = 'nLep20 >= 2 && mt >= 100000 && chanDilep == 2 && nBJets20 == 0 && nJets20 >= 2 && met > 20000 && met < 120000'
  # from HistFitter 6/11/2013
  #nLep20>=2 && nBJets20==0 && nJets20>=2 && met>20000 && met<120000 && chanDilep==2 && mt>=100000
  weight = blindWeight(weight)
  from comparator import ComparisonOptions
  options = ComparisonOptions()
  options.loadHistFitterStyle()
  options.stackMaxScaleFactor = 1000
  options.legX1 = 0.58; options.legY1 = 0.65
  options.legX2 = 0.90; options.legY2 = 0.95
  options.useLinearScale = True
  options.backgroundColor = globalOptions.backgroundColor
  appendComparison(comparisons, name, weight, 'meff/1000', 'm_{eff}', 'GeV', 100, 800, 7, options=options)

def appendAgreementDebuggingComparisons(comparisons):
  baseweight = '(nLooseLep == 2) * (mll < 81000 || mll > 101000) * (nJets40 > 1)'
  for name, weight in channelNameAndWeights('removeCF', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)
 
def appendCustomLepPtPlots(comparisons):
  basename = '5gev'
  #baseweight = '(nLooseLep == 2)'
  baseweight = ''
  for name, weight in channelNameAndWeights(basename, baseweight):
    nameWithLeadingHyphen = '-'+name
    appendComparison(comparisons, 'lep1Pt'+nameWithLeadingHyphen, weight, 'lep1Pt/1000', '1st hardest lepton p_{T}', 'GeV', 0, 150, 30)
    appendComparison(comparisons, 'lep2Pt'+nameWithLeadingHyphen, weight, 'lep2Pt/1000', '2nd hardest lepton p_{T}', 'GeV', 0, 150, 30)

def append3LEWK_VR0noZa(comparisons):
  baseweight = '(nLep10 == 3) * ((mllSFOS1 < 81000 || mllSFOS1 > 101000) && (mllSFOS2 < 81000 || mllSFOS2 > 101000)) * (met > 30000) * (met < 50000)'
  for name, weight in channelNameAndWeights('VR0noZa', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    #appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def append3LEWK_VR0noZb(comparisons):
  # First attempt - this is with 50<mllSFOS<81GeV
  #baseweight = '(nLep10 == 3) * ((mllSFOS1 > 50000 && mllSFOS1 < 81000) || (mllSFOS2 > 50000 && mllSFOS2 < 81000)) * (met > 50000) * (nBJets20 >= 1)'
  baseweight = '(nLep10 == 3) * (mllSFOS1 > 0 || mllSFOS2 > 0) * ((mllSFOS1 < 81000 || mllSFOS1 > 101000) && (mllSFOS2 < 81000 || mllSFOS2 > 101000)) * (mlll < 81000 || mlll > 101000) * (met > 50000) * (nBJets20 >= 1)'
  for name, weight in channelNameAndWeights('VR0noZb', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    #appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def append3LEWK_VR0noZb2b(comparisons):
  baseweight = '(nLep10 == 3) * (mllSFOS1 > 0 || mllSFOS2 > 0) * ((mllSFOS1 < 81000 || mllSFOS1 > 101000) && (mllSFOS2 < 81000 || mllSFOS2 > 101000)) * (mlll < 81000 || mlll > 101000) * (met > 50000) * (nBJets20 == 2)'
  for name, weight in channelNameAndWeights('VR0noZb2b', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)

def append3LEWK_VR0noZb3b(comparisons):
  baseweight = '(nLep10 == 3) * (mllSFOS1 > 0 || mllSFOS2 > 0) * ((mllSFOS1 < 81000 || mllSFOS1 > 101000) && (mllSFOS2 < 81000 || mllSFOS2 > 101000)) * (mlll < 81000 || mlll > 101000) * (met > 50000) * (nBJets20 == 3)'
  for name, weight in channelNameAndWeights('VR0noZb3b', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)

def append3LEWK_VR0noZbhighpt(comparisons):
  baseweight = '(lep1Pt > 20000) * (nLep15 == 3) * (mllSFOS1 > 0 || mllSFOS2 > 0) * ((mllSFOS1 < 81000 || mllSFOS1 > 101000) && (mllSFOS2 < 81000 || mllSFOS2 > 101000)) * (mlll < 81000 || mlll > 101000) * (met > 50000) * (nBJets20 >= 1)'
  for name, weight in channelNameAndWeights('VR0noZbhighpt', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)

def append3LEWK_VR0noZb2bhighpt(comparisons):
  baseweight = '(lep1Pt > 20000) * (nLep15 == 3) * (mllSFOS1 > 0 || mllSFOS2 > 0) * ((mllSFOS1 < 81000 || mllSFOS1 > 101000) && (mllSFOS2 < 81000 || mllSFOS2 > 101000)) * (mlll < 81000 || mlll > 101000) * (met > 50000) * (nBJets20 == 2)'
  for name, weight in channelNameAndWeights('VR0noZb2bhighpt', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)

def append3LEWK_VR0noZb3bhighpt(comparisons):
  baseweight = '(lep1Pt > 20000) * (nLep15 == 3) * (mllSFOS1 > 0 || mllSFOS2 > 0) * ((mllSFOS1 < 81000 || mllSFOS1 > 101000) && (mllSFOS2 < 81000 || mllSFOS2 > 101000)) * (mlll < 81000 || mlll > 101000) * (met > 50000) * (nBJets20 == 3)'
  for name, weight in channelNameAndWeights('VR0noZb3bhighpt', baseweight):
    appendComparisonsForNameAndWeight(comparisons, name, weight)




def addFlavourLabels(histogram):
  for tlConfig in threeLeptonTLConfigs():
    label = tlConfig.asFlavourString()
    binNumber = 1 + tlConfig.asBitmask()
    histogram.GetXaxis().SetBinLabel(binNumber, label)
    histogram.GetXaxis().SetBinLabel(9, '>3 lep')

def appendFourLeptonFlavourLables(histogram):
  for tlConfig in fourLeptonTLConfigs():
    label = tlConfig.asFlavourString()
    binNumber = 1 + tlConfig.asBitmask()
    histogram.GetXaxis().SetBinLabel(binNumber, label)
    histogram.GetXaxis().SetBinLabel(17, '>4 lep')

def addTLLabels(histogram):
  for tlConfig in threeLeptonTLConfigs():
    label = tlConfig.asString()
    binNumber = 1 + tlConfig.asBitmask()
    histogram.GetXaxis().SetBinLabel(binNumber, label)

class TLConfig(object):
  def __init__(self, leptonTightList=None):
    if leptonTightList == None:
      leptonTightList = []
    self._leptonTightList = leptonTightList

  def __eq__(self, other):
    numLeptonsInSelf = len(self._leptonTightList)
    numLeptonsInOther = len(other._leptonTightList)
    if numLeptonsInSelf != numLeptonsInOther:
      return False
    for i in range(numLeptonsInSelf):
      if self._leptonTightList[i] != other._leptonTightList[i]:
        return False
    return True

  def asBitmask(self):
    result = 0
    leptonIndex = 0
    for isTight in self._leptonTightList:
      if isTight:
        result += 1 << leptonIndex
      leptonIndex += 1
    return result

  def asString(self):
    result = ''
    for isTight in self._leptonTightList:
      if isTight:
        result += 'T'
      else:
        result += 'L'
    return result

  def asFlavourString(self):
    result = self.asString()
    result = result.replace('T', 'E')
    result = result.replace('L', 'M')
    return result

def threeLeptonTLConfigs():
  configLists = [[0,0,0], [1,0,0], [0,1,0], [0,0,1], [1,1,0], [1,0,1], [0,1,1], [1,1,1]]
  return tlConfigsFromLeptonTightLists(configLists)

def fourLeptonTLConfigs():
  import itertools
  configLists = list(set(itertools.permutations([0,0,0,0]))) 
  configLists += list(set(itertools.permutations([1,0,0,0]))) 
  configLists += list(set(itertools.permutations([1,1,0,0]))) 
  configLists += list(set(itertools.permutations([1,1,1,0]))) 
  configLists += list(set(itertools.permutations([1,1,1,1]))) 
  return tlConfigsFromLeptonTightLists(configLists)

def threeLeptonWithTwoTightTLConfigs():
  configLists = [[1,1,0], [1,0,1], [0,1,1], [1,1,1]]
  return tlConfigsFromLeptonTightLists(configLists)

def tlConfigsFromLeptonTightLists(leptonTightLists):
  result = []
  for leptonTightList in leptonTightLists:
    result.append(TLConfig(leptonTightList))
  return result


if __name__ == '__main__':
  main()

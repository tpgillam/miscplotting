#!/usr/bin/env python 
import sys
if '-b' not in sys.argv:
  sys.argv.append('-b')
  import ROOT
  sys.argv = sys.argv[:-1]

from gillam.rootcolours import solarized
from comparator import ComparisonOptions
globalOptions = ComparisonOptions()

def main():
  #treePath = '/r02/atlas/gillam/histFitterRPV/20130302RPV-nominal_1'
  #treePath = '/r02/atlas/gillam/histFitterRPV/20130604RPV-bJetMMTest_41'
  #outputDir = 'plots_bJetMMTest_41'
  #treePath = '/r02/atlas/gillam/histFitterRPV/20130812RPV-bJetMMTest_47'
  #outputDir = 'plots_bJetMMTest_47'
  treePath = '/r02/atlas/gillam/histFitterRPV/20130812RPV-bJetMMTest_48'
  outputDir = 'plots_bJetMMTest_48'

  import ROOT
  ROOT.gROOT.SetBatch(True)
  ROOT.gROOT.LoadMacro("AtlasStyle.C")
  ROOT.gROOT.LoadMacro("AtlasLabels.C")
  ROOT.SetAtlasStyle()

  import os.path
  backgroundsFile = ROOT.TFile(os.path.join(treePath, 'backgroundsNoFR.root'))
  #fakeEstimateFile = ROOT.TFile(os.path.join(treePath, 'fakeEstimate.root'))
  #chargeFlipFile = ROOT.TFile(os.path.join(treePath, 'chargeFlip.root'))
  #bJetMMFile = ROOT.TFile(os.path.join(treePath, 'bJetMMEstimate_skimmed.root'))
  bJetMMFile = ROOT.TFile(os.path.join(treePath, 'bJetMMEstimate.root'))

  from comparator import MCBackground, MCBackgroundList
  backgroundDict = {}
  for systematicPostfix in ['nom']:
    mcBackgrounds = MCBackgroundList()
    diBoson = MCBackground(backgroundsFile.Get('diBoson_'+systematicPostfix), name='diBoson', label='Sherpa diBoson', color=ROOT.kRed+3)
    ttbarBB = MCBackground(backgroundsFile.Get('ttbarBB_'+systematicPostfix), name='ttbarBB', label='Alpgen ttbarbb', color=ROOT.kOrange)
    topV = MCBackground(backgroundsFile.Get('topV_'+systematicPostfix), name='topV', label='MadGraph ttbar+V', color=ROOT.kRed-2)
    topMcAtNLO = MCBackground(backgroundsFile.Get('top_'+systematicPostfix), name='top', label='MC@NLO ttbar', color=ROOT.kOrange+2)
    topPowheg = MCBackground(backgroundsFile.Get('topPowheg_'+systematicPostfix), name='topPowheg', label='Powheg ttbar', color=ROOT.kOrange+2)
    zJetsMassiveB = MCBackground(backgroundsFile.Get('zJetsMassiveB_'+systematicPostfix), name='zJetsMassiveB', label='Sherpa Z+jets (massive B)', color=ROOT.kViolet-6)

    diBoson.addWeight('(has3BQuarks == 1)')
    ttbarBB.addWeight('(has3BQuarks == 1)')
    topV.addWeight('(has3BQuarks == 1)')
    topMcAtNLO.addWeight('(has3BQuarks == 1)')
    topPowheg.addWeight('(has3BQuarks == 1)')
    zJetsMassiveB.addWeight('(has3BQuarks == 1)')
    mcBackgrounds.append(diBoson)
    #mcBackgrounds.append(topMcAtNLO)
    #mcBackgrounds.append(ttbarBB)
    mcBackgrounds.append(topV)
    mcBackgrounds.append(zJetsMassiveB)
    #mcBackgrounds.append(MCBackground(fakeEstimateFile.Get('fakes_nom'), name='fakes', label='Fake leptons', color=solarized.green, isDataDriven=True))
    #mcBackgrounds.append(MCBackground(chargeFlipFile.Get('chargeFlip_nom'), name='chargeFlip', label='Charge flipped leptons', color=solarized.violet, isDataDriven=True))
    mcBackgrounds.append(topPowheg)
    

    bJetMM = MCBackground(bJetMMFile.Get('bJetMM_nom'), name='bJetMM', label='B-Jet MM', color=ROOT.kCyan+1, isDataDriven=True)
    bJetMM.addWeightWithSystematic('BMMWGT', 'bMMWgt', 'bMMWgt_UP', None)
    #bJetMM.addWeight('bMMWgt')
    mcBackgrounds.append(bJetMM)
    backgroundDict[systematicPostfix] = mcBackgrounds

  egammaPath = os.path.join(treePath, 'egamma.root')
  muonPath = os.path.join(treePath, 'muon.root')
  jettauetmissPath = os.path.join(treePath, 'jettauetmiss.root')

  dataChain = ROOT.TChain('data')
  dataChain.AddFile(egammaPath)
  dataChain.AddFile(muonPath)
  dataChain.AddFile(jettauetmissPath)

  from comparator import ComparisonInputs
  comparisonInputs = ComparisonInputs(dataChain, backgroundDict['nom'])
  nominalWeights = ['mcWgt', 'pileupWgt', 'trigWgt', 'eGammaWgt', 'muonWgt', 'lumiScaling' ,'bTagWgt']
  for weight in nominalWeights:
    comparisonInputs.addWeight(weight)
  #comparisonInputs.addWeightWithSystematic('CHARGEFLIP', 'chargeFlipWgt', 'chargeFlipWgt_UP', mcBackgroundNamesToUse=['chargeFlip', 'fakes'])
  #comparisonInputs.addWeightWithSystematic('FAKEEL', 'fakeLeptWgt', 'fakeLeptWgt_El_UP', mcBackgroundNamesToUse='fakes')
  #comparisonInputs.addWeightWithSystematic('FAKEMU', 'fakeLeptWgt', 'fakeLeptWgt_Mu_UP', mcBackgroundNamesToUse='fakes')
  

  comparisons = []
  appendBasicComparisons(comparisons)
  append3JetComparisons(comparisons)
  appendSR3bComparisons(comparisons)

  from comparator import makeComparisons
  makeComparisons(outputDir, comparisons, comparisonInputs, None, extraLumiWeight=20661.0/20694.9)

  backgroundsFile.Close()
  #fakeEstimateFile.Close()

def appendBasicComparisons(comparisons):
  for name, weight in channelNameAndWeights('3b', '(nBJets20 > 2)'):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def append3JetComparisons(comparisons):
  for name, weight in channelNameAndWeights('3b3j', '(nBJets20 > 2 && nJets20 == 3)'):
    appendComparisonsForNameAndWeight(comparisons, name, weight)
    appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight)

def channelNameAndWeights(basename=None, baseweight=None):
  for lepChan in [None, 0, 1, 2]:
    if basename is None or basename == '':
      name = ''
    elif lepChan is None:
      name = basename
    else:
      name = basename+'-'
    if lepChan is not None:
      if lepChan == 0:
        name += 'ee'
      elif lepChan == 1:
        name += 'em'
      elif lepChan == 2:
        name += 'mm'

    if baseweight is None or baseweight == '':
      weight = '1'
    else:
      weight = baseweight
    if lepChan is not None:
      weight += ' * (chanDilep=={0})'.format(lepChan)
    yield name, weight

def appendSR3bComparisons(comparisons):
  name = 'SR3b'
  weight = 'nLep15 >= 2 && nJets40 >= 5 && nBJets20 >= 3'
  nameCR = 'CR3b'
  weightCR = 'nLep15 >= 2 && nJets40 < 5 && nBJets20 >= 3'
  nameCR2 = 'CR3b2'
  weightCR2 = 'nLep15 >= 2 && nBJets20 >= 3 && meff < 190000'
  nameCR3 = 'CR3b3'
  weightCR3 = 'nLep15 >= 2 && nJets40 >= 5 && nBJets20 >= 3 && meff < 190000'
  name4j = 'SR3b4j'
  weight4j = 'nLep15 >= 2 && nJets40 >= 4 && nBJets20 >= 3'

  from comparator import ComparisonOptions
  options = ComparisonOptions()
  options.blinded = True
  options.useLinearScale = True
  options.backgroundColor = globalOptions.backgroundColor
  appendComparison(comparisons, name, weight, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, 1, options=options)
  appendComparison(comparisons, name4j, weight4j, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, 1, options=options)
  import copy
  options2 = copy.deepcopy(options)
  options2.blinded = False
  appendComparison(comparisons, nameCR, weightCR, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, 1, options=options2)
  appendComparison(comparisons, nameCR2, weightCR2, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, 1, options=options2)
  appendComparison(comparisons, nameCR3, weightCR3, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, 1, options=options2)

def appendComparisonsForNameAndWeight(comparisons, name, weight, binMultiplier=1, options=None):
  if name == '':
    nameWithLeadingHyphen = ''
  else:
    nameWithLeadingHyphen = '-' + name
  appendComparison(comparisons, 'mll'+nameWithLeadingHyphen, weight, 'mll/1000', 'm_{ll}', 'GeV', 0, 600, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'mllSFOS1'+nameWithLeadingHyphen, weight, 'mllSFOS1/1000', 'm_{SFOS1}', 'GeV', 0, 600, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'mllSFOS2'+nameWithLeadingHyphen, weight, 'mllSFOS2/1000', 'm_{SFOS2}', 'GeV', 0, 600, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'met'+nameWithLeadingHyphen, weight, 'met/1000', 'E^{T}_{miss}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'meff'+nameWithLeadingHyphen, weight, 'meff/1000', 'm_{eff}', 'GeV', 0, 1500, int(round(15*binMultiplier)), options)
  appendComparison(comparisons, 'mt'+nameWithLeadingHyphen, weight, 'mt/1000', 'm_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'nJets20'+nameWithLeadingHyphen, weight, 'nJets20', 'nJets > 20 GeV', None, 0, 12, 12, options)
  appendComparison(comparisons, 'nBJets20'+nameWithLeadingHyphen, weight, 'nBJets20', 'nBJets > 20 GeV', None, 0, 12, 12, options)
  appendComparison(comparisons, 'lep1Pt'+nameWithLeadingHyphen, weight, 'lep1Pt/1000', '1st hardest lepton p_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'lep2Pt'+nameWithLeadingHyphen, weight, 'lep2Pt/1000', '2nd hardest lepton p_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'lep1Eta'+nameWithLeadingHyphen, weight, 'lep1Eta', '1st hardest lepton \eta', '', -3, 3, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'lep2Eta'+nameWithLeadingHyphen, weight, 'lep2Eta', '2nd hardest lepton \eta', '', -3, 3, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'jet1Pt'+nameWithLeadingHyphen, weight, 'jet1Pt/1000', '1st hardest jet p_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'jet2Pt'+nameWithLeadingHyphen, weight, 'jet2Pt/1000', '2nd hardest jet p_{T}', 'GeV', 0, 250, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'jet3Pt'+nameWithLeadingHyphen, weight, 'jet3Pt/1000', '3rd hardest jet p_{T}', 'GeV', 0, 150, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'bJet1Pt'+nameWithLeadingHyphen, weight, 'bJet1Pt/1000', '1st hardest b-jet p_{T}', 'GeV', 0, 500, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'bJet2Pt'+nameWithLeadingHyphen, weight, 'bJet2Pt/1000', '2nd hardest b-jet p_{T}', 'GeV', 0, 250, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'bJet3Pt'+nameWithLeadingHyphen, weight, 'bJet3Pt/1000', '3rd hardest b-jet p_{T}', 'GeV', 0, 150, int(round(10*binMultiplier)), options)
  appendComparison(comparisons, 'jet1Eta'+nameWithLeadingHyphen, weight, 'jet1Eta', '1st hardest jet \eta', '', -3, 3, int(round(12*binMultiplier)), options)
  appendComparison(comparisons, 'jet2Eta'+nameWithLeadingHyphen, weight, 'jet2Eta', '2nd hardest jet \eta', '', -3, 3, int(round(12*binMultiplier)), options)
  appendComparison(comparisons, 'jet3Eta'+nameWithLeadingHyphen, weight, 'jet3Eta', '3rd hardest jet \eta', '', -3, 3, int(round(12*binMultiplier)), options)


def appendComparison(comparisons, fullName, weight, variable, xLabel, units, xMin, xMax, numBins, options=None):
  if units is None:
    title = ';'+xLabel+';Events'
  elif units == '':
    unitsPerBin = int(round((xMax-xMin)/numBins))
    title = ';'+xLabel+';Events / {0}'.format(unitsPerBin)
  else:
    unitsPerBin = int(round((xMax-xMin)/numBins))
    title = ';'+xLabel+' [{0}];Events / {1} {0}'.format(units, unitsPerBin)
  from comparator import Comparison
  if options is None:
    options = globalOptions
  comparisons.append(Comparison(variable, ROOT.TH1D(fullName, title, numBins, xMin, xMax), weight, options))

def appendFineBinnedComparisonsForNameAndWeight(comparisons, name, weight):
  if name == '':
    modifiedName = 'fineBin'
  else:
    modifiedName = 'fineBin-'+name
  appendComparisonsForNameAndWeight(comparisons, modifiedName, weight, binMultiplier=5)

if __name__ == '__main__':
  main()

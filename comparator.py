class Systematic(object):
  def __init__(self, name, mcBackground):
    self.name = name
    self.mcBackground = mcBackground
    self.namePrefix = None
    self.refHistogram = None

  def __eq__(self, other):
    return self.name == other.name

  def clone(self):
    raise NotImplementedError
 
  def setNameAndReferenceHistogram(self, namePrefix, refHistogram):
    self.namePrefix = namePrefix
    self.refHistogram = refHistogram

  def makeHistograms(self):
    raise NotImplementedError

  def _cloneHistogramToTarget(self, targetName, namePostfix):
    newName = self._getHistogramNamePrefix() + namePostfix
    target = getattr(self, targetName)
    if target is not None:
      target.Delete()
    setattr(self, targetName, self.refHistogram.Clone(newName))
    target = getattr(self, targetName)
    target.SetLineColor(self.mcBackground.color)
    target.SetFillColor(self.mcBackground.color)

  def _getHistogramNamePrefix(self):
    return self.namePrefix + self.mcBackground.name + self.name

  def getProjections(self, variable, cuts):
    raise NotImplementedError

  def finaliseUpDownHistograms(self):
    raise NotImplementedError


class TreeSystematic(Systematic):
  def __init__(self, name, mcBackground, treeUP, treeDOWN=None):
    super(TreeSystematic, self).__init__(name, mcBackground)
    self.isSymmetric = treeDOWN is None
    self.treeUP = treeUP
    self.treeDOWN = treeDOWN
    self.histogramUP = None
    self.histogramDOWN = None

  def clone(self):
    return TreeSystematic(self.name, self.mcBackground, self.treeUP, self.treeDOWN)

  def makeHistograms(self):
    self._cloneHistogramToTarget('histogramUP', 'UP')
    if not self.isSymmetric:
      self._cloneHistogramToTarget('histogramDOWN', 'DOWN')

  def getProjections(self, variable, cuts):
    projections = []
    from multiprojector import Projection
    projections.append(Projection(self.treeUP, self.histogramUP, variable, cuts))
    if not self.isSymmetric:
      projections.append(Projection(self.treeDOWN, self.histogramDOWN, variable, cuts))
    return projections

  def finaliseUpDownHistograms(self):
    pass


class WeightSystematic(Systematic):
  def __init__(self, name, mcBackground, weightModifier, weightModifierUP, weightModifierDOWN=None):
    super(WeightSystematic, self).__init__(name, mcBackground)
    self.isSymmetric = (weightModifierDOWN is None)
    self.weightModifier = weightModifier
    self.weightModifierUP = weightModifierUP
    self.weightModifierDOWN = weightModifierDOWN
    self.histogramUP = None
    self.histogramDOWN = None
    self._histogramNominal = None
    self._histogramUncertaintyUPSquared = None
    self._histogramUncertaintyDOWNSquared = None
  
  def clone(self):
    return WeightSystematic(self.name, self.mcBackground, self.weightModifier, self.weightModifierUP, self.weightModifierDOWN)

  def makeHistograms(self):
    self._cloneHistogramToTarget('_histogramNominal', 'NOM')
    self._cloneHistogramToTarget('histogramUP', 'UP')
    self._cloneHistogramToTarget('_histogramUncertaintyUPSquared', 'UUPSQR')
    if not self.isSymmetric:
      self._cloneHistogramToTarget('histogramDOWN', 'DOWN')
      self._cloneHistogramToTarget('_histogramUncertaintyDOWNSquared', 'UDOWNSQR')

  def getProjections(self, variable, cuts):
    cuts = self._removeNominalWeight(cuts)
    uncertaintyUPSquaredWeight = 'pow({0}, 2) * pow({1} - {2}, 2)'.format(cuts, self.weightModifierUP, self.weightModifier)
    projections = []
    from multiprojector import Projection
    tree = self.mcBackground.tree
    nominalWeight = '{0} * {1}'.format(cuts, self.weightModifier)
    uncertaintyUPSquaredWeight = 'pow({0}*({1} - {2}), 2)'.format(cuts, self.weightModifierUP, self.weightModifier)
    projections.append(Projection(tree, self._histogramNominal, variable, nominalWeight))
    projections.append(Projection(tree, self._histogramUncertaintyUPSquared, variable, uncertaintyUPSquaredWeight))
    if not self.isSymmetric:
      uncertaintyDOWNSquaredWeight = 'pow({0}*({1} - {2}), 2)'.format(cuts, self.weightModifierDOWN, self.weightModifier)
      projections.append(Projection(tree, self._histogramUncertaintyDOWNSquared, variable, uncertaintyDOWNSquaredWeight))
    return projections

  def _removeNominalWeight(self, cuts):
    if self.weightModifier in cuts:
      cuts = cuts.replace(self.weightModifier, '1')
    return cuts

  def finaliseUpDownHistograms(self):
    nominal = self._histogramNominal
    self._squareRootHistogram(self._histogramUncertaintyUPSquared)
    self._addHistogramsIntoTarget(nominal, self._histogramUncertaintyUPSquared, self.histogramUP)
    if not self.isSymmetric:
      self._squareRootHistogram(self._histogramUncertaintyDOWNSquared)
      self._subtractHistogramsIntoTarget(nominal, self._histogramUncertaintyDOWNSquared, self.histogramUP)

  def _squareRootHistogram(self, histogram):
    for i in range(1, histogram.GetNbinsX() + 1):
      wSquared = histogram.GetBinContent(i)
      from math import sqrt
      histogram.SetBinContent(i, sqrt(wSquared))

  def _addHistogramsIntoTarget(self, hist1, hist2, target):
    for i in range(1, hist1.GetNbinsX() + 1):
      x1 = hist1.GetBinContent(i)
      x2 = hist2.GetBinContent(i)
      target.SetBinContent(i, x1 + x2)

  def _subtractHistogramsIntoTarget(self, hist1, hist2, target):
    for i in range(1, hist1.GetNbinsX() + 1):
      x1 = hist1.GetBinContent(i)
      x2 = hist2.GetBinContent(i)
      target.SetBinContent(i, x1 - x2)


class FullyCorrelatedWeightSystematic(Systematic):
  def __init__(self, name, mcBackground, weightModifier, weightModifierUP, weightModifierDOWN=None):
    super(FullyCorrelatedWeightSystematic, self).__init__(name, mcBackground)
    self.isSymmetric = (weightModifierDOWN is None)
    self.weightModifier = weightModifier
    self.weightModifierUP = weightModifierUP
    self.weightModifierDOWN = weightModifierDOWN
    self.histogramUP = None
    self.histogramDOWN = None
  
  def clone(self):
    return FullyCorrelatedWeightSystematic(self.name, self.mcBackground, self.weightModifier, self.weightModifierUP, self.weightModifierDOWN)

  def makeHistograms(self):
    self._cloneHistogramToTarget('histogramUP', 'UP')
    if not self.isSymmetric:
      self._cloneHistogramToTarget('histogramDOWN', 'DOWN')

  def getProjections(self, variable, cuts):
    cuts = self._removeNominalWeight(cuts)
    tree = self.mcBackground.tree
    cutsUP = '{0} * {1}'.format(cuts, self.weightModifierUP)
    projections = []
    from multiprojector import Projection
    projections.append(Projection(tree, self.histogramUP, variable, cutsUP))
    if not self.isSymmetric:
      cutsDOWN = '{0} * {1}'.format(cuts, self.weightModifierDOWN)
      projections.append(Projection(tree, self.histogramDOWN, variable, cutsDOWN))
    return projections

  def _removeNominalWeight(self, cuts):
    if self.weightModifier in cuts:
      cuts = cuts.replace(self.weightModifier, '1')
    return cuts

  def finaliseUpDownHistograms(self):
    pass


class MCBackground(object):
  def __init__(self, tree, name=None, label=None, color=None, isDataDriven=False, options=None, extraCut=None):
    self.tree = tree
    self.options = options
    if self.options is None:
      self.options = ComparisonOptions()

    self.name = name
    if name is None:
      self.name = tree.GetName()

    self.label = label
    if label is None:
      self.label = self.name

    self.color = color
    if color is None:
      import ROOT
      colorList = [ROOT.kGreen+2, ROOT.kRed, ROOT.kBlue+2, ROOT.kRed-2, ROOT.kYellow-9, ROOT.kPink+2, ROOT.kRed+4, ROOT.kGreen-5]
      from random import choice
      self.color = choice(colorList)
    self.isDataDriven = isDataDriven

    self.histogram = None
    self.systematics = []
    self.weightToSystematics = {}
    self.extraWeights = []
    self.extraCut = extraCut

  def addWeight(self, weightModifier):
    weightModifier = weightModifier.strip()
    if weightModifier not in self.extraWeights:
      self.extraWeights.append(weightModifier)

  def addWeightWithSystematic(self, name, weightModifier, weightModifierUP, weightModifierDOWN=None):
    if weightModifierUP is None and weightModifierDOWN is None:
      self.addWeight(weightModifier)
    else:
      systematic = FullyCorrelatedWeightSystematic(name, self, weightModifier, weightModifierUP, weightModifierDOWN)
      self.systematics.append(systematic)
      if weightModifier not in self.weightToSystematics:
        self.weightToSystematics[weightModifier] = []
      self.weightToSystematics[weightModifier].append(systematic)

  def addTreeSystematic(self, name, treeUP, treeDOWN=None):
    systematic = TreeSystematic(name, self, treeUP, treeDOWN)
    self.systematics.append(systematic)
  
  def totalUncertaintyGraph(self):
    import ROOT
    graph = ROOT.TGraphAsymmErrors(self.histogram)
    for systematic in self.systematics:
      for i in range(0, self.histogram.GetNbinsX()):
        from math import sqrt
        eUP = sqrt(graph.GetErrorYhigh(i)**2 + (systematic.histogramUP.GetBinContent(i+1) - self.histogram.GetBinContent(i+1))**2)
        graph.SetPointEYhigh(i, eUP)
        if systematic.isSymmetric:
          eDOWN = sqrt(graph.GetErrorYlow(i)**2 + (systematic.histogramUP.GetBinContent(i+1) - self.histogram.GetBinContent(i+1))**2)
        else:
          eDOWN = sqrt(graph.GetErrorYlow(i)**2 + (systematic.histogramDOWN.GetBinContent(i+1) - self.histogram.GetBinContent(i+1))**2)
        graph.SetPointEYlow(i, eDOWN)
    return graph

  def clone(self):
    new = MCBackground(self.tree, self.name, self.label, self.color, self.isDataDriven, self.options, self.extraCut)
    for weight in self.extraWeights:
      new.extraWeights.append(weight)

    for weight in self.weightToSystematics:
      new.weightToSystematics[weight] = []
      for systematic in self.weightToSystematics[weight]:
        newSystematic = systematic.clone()
        new.weightToSystematics[weight].append(newSystematic)
        new.systematics.append(newSystematic)
  
    for systematic in self.systematics:
      if systematic in new.systematics:
        continue
      new.systematics.append(systematic.clone())
    return new

  def makeHistograms(self, refHistogram, namePrefix):
    if self.histogram is not None:
      self.histogram.Delete()
    self.histogram = refHistogram.Clone(namePrefix+self.name)

    if self.options.useBlackLinesInStack:
      import ROOT
      self.histogram.SetLineColor(ROOT.kBlack)
    else:
      self.histogram.SetLineColor(self.color)
    self.histogram.SetLineWidth(self.options.stackLineWidth)
    self.histogram.SetFillColor(self.color)
    for systematic in self.systematics:
      systematic.setNameAndReferenceHistogram(namePrefix+self.name+systematic.name, refHistogram)
      systematic.makeHistograms()

  def getProjections(self, variable, cuts):
    cuts = '*'.join(['('+cuts+')',]+self.extraWeights)
    if self.extraCut is not None:
      cuts += '* ({0})'.format(self.extraCut)
    cutsWithAllWeights = '*'.join(['('+cuts+')',]+self.weightToSystematics.keys())

    projections = []
    from multiprojector import Projection
    projections.append(Projection(self.tree, self.histogram, variable, cutsWithAllWeights))

    for systematic in self.systematics:
      cutsWithoutSystematicWeight = '*'.join(['('+cuts+')',]+[x for x in self.weightToSystematics if systematic not in self.weightToSystematics[x]])
      projections += systematic.getProjections(variable, cutsWithoutSystematicWeight)
    return projections


class SummarySystematic(object):
  def __init__(self, name, histogramUP, histogramDOWN):
    self.name = name
    self.isSymmetric = (histogramDOWN is None)
    self.histogramUP = histogramUP
    self.histogramDOWN = histogramDOWN


class NameLookupList(list):
  def __getitem__(self, key):
    if isinstance(key, str):
      for item in self:
        if item.name == key:
          return item
      raise KeyError
    return list.__getitem__(self, key)

  def __setitem__(self, key, value):
    if isinstance(key, str):
      for i in range(len(self)):
        if self[i].name == key:
          self[i] = value
          return
      raise KeyError
    return list.__setitem__(self, key, value)

  def hasName(self, key):
    for item in self:
      if item.name == key:
        return True
    return False
  

class MCBackgroundList(NameLookupList):
  def clone(self):
    new = MCBackgroundList()
    for mcBackground in self:
      new.append(mcBackground.clone())
    return new

  def makeHistograms(self, refHistogram, namePrefix):
    self.systematics = NameLookupList()
    for mcBackground in self:
      mcBackground.makeHistograms(refHistogram, namePrefix)
      for systematic in mcBackground.systematics:
        if self.systematics.hasName(systematic.name):
          continue
        histogramUP = refHistogram.Clone(namePrefix+'TotalSyst'+systematic.name+'UP')
        histogramDOWN = None
        if not systematic.isSymmetric:
          histogramDOWN = refHistogram.Clone(namePrefix+'TotalSyst'+systematic.name+'DOWN')
        self.systematics.append(SummarySystematic(systematic.name, histogramUP, histogramDOWN))

  def getProjections(self, variable, cuts, extraLumiWeight=None):
    projections = []
    for mcBackground in self:
      cutsWithWeightIfNecessary = cuts
      if not mcBackground.isDataDriven and extraLumiWeight is not None:
        cutsWithWeightIfNecessary += '* {0}'.format(extraLumiWeight)
      projections += mcBackground.getProjections(variable, cutsWithWeightIfNecessary)
    return projections

  def updateSummarySystematics(self):
    for mcBackground in self:
      for systematic in mcBackground.systematics:
        systematic.finaliseUpDownHistograms()
        summarySystematic = self.systematics[systematic.name]
        summarySystematic.histogramUP.Add(systematic.histogramUP)
        if not summarySystematic.isSymmetric:
          summarySystematic.histogramDOWN.Add(systematic.histogramDOWN)
    self._addNominalComponentsForBackgroundsWithoutSystematic()

  def _addNominalComponentsForBackgroundsWithoutSystematic(self):
    for systematic in self.systematics:
      for mcBackground in self:
        if systematic not in mcBackground.systematics:
          self.systematics[systematic.name].histogramUP.Add(mcBackground.histogram)


class ComparisonInputs(object):
  def __init__(self, dataTree, mcBackgrounds, dataWeights=None):
    self.dataTree = dataTree
    self.mcBackgrounds = self._castToMCBackgroundList(mcBackgrounds)
    self.dataWeights = dataWeights

  def clone(self):
    return ComparisonInputs(self.dataTree, self.mcBackgrounds.clone())

  def addTreeSystematic(self, name, mcBackgroundsUP, mcBackgroundsDOWN=None):
    mcBackgroundsUP = self._castToMCBackgroundList(mcBackgroundsUP)
    mcBackgroundsDOWN = self._castToMCBackgroundList(mcBackgroundsDOWN)

    for mcBackground in self.mcBackgrounds:
      treeUP = mcBackgroundsUP[mcBackground.name].tree
      treeDOWN = None
      if mcBackgroundsDOWN is not None:
        treeDOWN = mcBackgroundsDOWN[mcBackground.name].tree
      mcBackground.addTreeSystematic(name, treeUP, treeDOWN)

  def _castToMCBackgroundList(self, mcBackgrounds):
    result = mcBackgrounds
    if result is None:
      return None
    if len(mcBackgrounds) > 0:
      if not isinstance(mcBackgrounds[0], MCBackground):
        result = MCBackgroundList()
        for tree in mcBackgrounds:
          result.append(MCBackground(tree))
    return result

  def addWeightWithSystematic(self, name, weightModifier, weightModifierUP, weightModifierDOWN=None, mcBackgroundNamesToUse=None):
    for mcBackground in self._relevantBackgrounds(mcBackgroundNamesToUse):
      mcBackground.addWeightWithSystematic(name, weightModifier, weightModifierUP, weightModifierDOWN)

  def addWeight(self, weightModifier, mcBackgroundNamesToUse=None):
    for mcBackground in self._relevantBackgrounds(mcBackgroundNamesToUse):
      mcBackground.addWeight(weightModifier)

  def _relevantBackgrounds(self, mcBackgroundNamesToUse):
    if mcBackgroundNamesToUse is not None and isinstance(mcBackgroundNamesToUse, str):
      mcBackgroundNamesToUse = [mcBackgroundNamesToUse,]
    for mcBackground in self.mcBackgrounds:
      if mcBackgroundNamesToUse is not None and mcBackground.name not in mcBackgroundNamesToUse:
        continue
      yield mcBackground


class ComparisonOptions(object):
  def __init__(self):
    self.loadDefaults()

  def loadDefaults(self):
    self.blinded = False
    self.useLinearScale = False
    self.moveOverflowToLastBin = False
    self.canvasWidth = 700
    self.canvasHeight = 500
    import ROOT
    self.backgroundColor = ROOT.kWhite
    self.leftMargin = None
    self.rightMargin = 0.1
    self.ratioTitle = 'Data / SM'
    self.ratioLineWidth = 2
    self.ratioLineColor = ROOT.kRed
    self.ratioLineStyle = 1
    self.ratioCentralLineStyle = 1
    self.ratioLinePositions = [1.0]
    self.ratioMin = 0.0
    self.ratioMax = 2.0
    self.ratioPadTopMargin = 0.05
    self.ratioPadBottomMargin = 0.5
    self.ratioTitleSizeX = 0.12
    self.ratioTitleOffsetX = None
    self.ratioLabelSizeX = 0.12
    self.ratioLabelOffsetX = 0.03
    self.ratioTitleSizeY = 0.12
    self.ratioTitleOffsetY = 0.5
    self.ratioLabelSizeY = 0.12
    self.ratioLabelOffsetY = None
    self.ratioDataLineWidth = 2
    self.ratioDataMarkerSize = None
    self.plotPadTopMargin = None
    self.plotPadBottomMargin = 0.0
    self.stackRoundLimitsToPower = False
    self.stackMaxScaleFactor = 10
    self.stackLineWidth = 2
    self.stackTitleOffset = 1.22
    self.stackTitleSize = None
    self.stackLabelSize = None
    self.plotDataLineWidth = None
    self.plotDataMarkerSize = None
    self.useBlackLinesInStack = False
    self.suppressXLinesInData = False
    self.atlasLabelEnabled = False
    self.atlasLabelText = ''
    self.atlasLabelX = 0.2
    self.atlasLabelY = 0.86
    self.atlasLabelTextSize = 0.04
    self.lumiLabelText = None
    self.lumiLabelX = 0.6; self.lumiLabelY = 0.5
    self.lumiLabelTextSize = 0.03
    self.regionLabelText = None
    self.regionLabelX = 0.8; self.regionLabelY = 0.8
    self.regionLabelTextSize = 0.02
    self.legDataMarkerSize = None
    self.legMCTotalLabel = 'Total background'
    self.legMCTotalAsJustFill = False
    self.legX1 = 0.60; self.legY1 = 0.65
    self.legX2 = 0.90; self.legY2 = 0.92
    self.legTextSize = 0.037

  def loadHistFitterStyle(self):
    self.moveOverflowToLastBin = True
    self.canvasWidth = 700
    self.canvasHeight = 602
    self.leftMargin = 0.099
    self.rightMargin = 0.045
    self.useBlackLinesInStack = True
    self.stackLineWidth = 1
    self.stackRoundLimitsToPower = True
    self.stackMaxScaleFactor = 100
    self.stackTitleOffset = 0.9
    self.stackTitleSize = 0.055
    self.stackLabelSize = 0.0555
    self.plotDataLineWidth = 1
    self.plotDataMarkerSize = 1.2
    self.suppressXLinesInData = True
    self.ratioTitle = 'Data / Exp.'
    self.ratioMax = 2.2
    import ROOT
    self.ratioLineColor = ROOT.kBlack
    self.ratioLineWidth = 1
    self.ratioLineStyle = 3
    self.ratioCentralLineStyle = 2
    self.ratioLinePositions = [0.5, 1.0, 1.5, 2.0]
    self.ratioPadTopMargin = 0.048
    self.ratioPadBottomMargin = 0.323
    self.ratioTitleSizeX = 0.132
    self.ratioTitleOffsetX = 1.0
    self.ratioLabelSizeX = 0.125
    self.ratioLabelOffsetX = 0.03
    self.ratioTitleSizeY = 0.132
    self.ratioTitleOffsetY = 0.38
    self.ratioLabelSizeY = 0.125
    self.ratioLabelOffsetY = 0.01
    self.ratioDataLineWidth = 1
    self.ratioDataMarkerSize = 1
    self.plotPadTopMargin = 0.014
    self.plotPadBottomMargin = 0.014
    self.atlasLabelEnabled = False
    self.atlasLabelText = False
    self.atlasLabelX = 0.1695
    self.atlasLabelY = 0.854
    self.atlasLabelTextSize = 0.0495
    self.lumiLabelText = None
    self.lumiLabelX = 0.1695; self.lumiLabelY = 0.762
    self.lumiLabelTextSize = 0.0441
    self.regionLabelText = None
    self.regionLabelX = 0.699; self.regionLabelY = 0.891
    self.regionLabelTextSize = 0.035
    self.legDataMarkerSize = 1
    self.legMCTotalLabel = 'SM Total'
    self.legMCTotalAsJustFill = False
    self.legX1 = 0.693; self.legY1 = 0.495
    self.legX2 = 0.968; self.legY2 = 0.87
    self.legTextSize = 0.035

class Comparison(object):
  def __init__(self, variable, histogram, cuts, options=None):
    histogram.Sumw2()
    self.name = histogram.GetName()
    self.variable = variable
    self._refHistogram = histogram
    self.dataHistogram = None
    self.mcStack = None
    self.mcTotalHistogram = None
    self.mcTotalGraph = None
    self.ratioMCHistogram = None
    self.ratioMCGraph = None
    self.ratioDataHistogram = None
    self.ratioDataGraph = None
    self.ratioZeroLine = None
    self.cuts = cuts
    if options is not None:
      self.options = options
    else:
      self.options = ComparisonOptions()

  def getProjections(self, comparisonInputs, dataWeight=None, extraLumiWeight=None):
    self.comparisonInputs = comparisonInputs.clone()
    self._makeHistograms()
    
    dataCuts = self.cuts
    if dataWeight is not None:
      dataCuts = '{0} * {1}'.format(self.cuts, dataWeight)
    if comparisonInputs.dataWeights is not None:
      dataCuts = '('+dataCuts+')'
      for weight in comparisonInputs.dataWeights:
        dataCuts += ' * {0}'.format(weight)

    from multiprojector import Projection
    projections = []
    projections.append(Projection(self.comparisonInputs.dataTree, self.dataHistogram, self.variable, dataCuts))
    projections += self.comparisonInputs.mcBackgrounds.getProjections(self.variable, self.cuts, extraLumiWeight)

    return projections

  def _includeOverlapInLastBin(self):
    for histogram in self._allInputHistograms():
      last = histogram.GetNbinsX()
      overflow = last + 1
      y1 = histogram.GetBinContent(last)
      uy1 = histogram.GetBinError(last)
      y2 = histogram.GetBinContent(overflow)
      uy2 = histogram.GetBinError(overflow)
      histogram.SetBinContent(last, y1+y2)
      from math import sqrt
      histogram.SetBinError(last, sqrt(uy1**2+uy2**2))

  def _allInputHistograms(self):
    yield self.dataHistogram
    for mcBackground in self.comparisonInputs.mcBackgrounds:
      yield mcBackground.histogram
      for systematic in mcBackground.systematics:
        for prop in systematic.__dict__:
          if not (prop.startswith('histogram') or prop.startswith('_histogram')):
            continue
          histCandidate = systematic.__dict__[prop]
          if histCandidate is None: continue
          yield histCandidate

  def makeComparison(self):
    if self.options.moveOverflowToLastBin:
      self._includeOverlapInLastBin()

    self.comparisonInputs.mcBackgrounds.updateSummarySystematics()

    for mcBackground in self.comparisonInputs.mcBackgrounds:
      self.mcTotalHistogram.Add(mcBackground.histogram)

    if self.options.blinded:
      dataHistogram = self.mcTotalHistogram
    else:
      dataHistogram = self.dataHistogram
    self.minimumY = min(self._getMin(dataHistogram), self._getMin(self.mcTotalHistogram))*0.1
    self.minimumY = max(self.minimumY, 0.01)
    self.maximumY = max(self._getMax(dataHistogram), self._getMax(self.mcTotalHistogram))*self.options.stackMaxScaleFactor
    if self.options.stackRoundLimitsToPower:
      import math
      logMinY = round(math.log(self.minimumY, 10))
      logMaxY = round(math.log(self.maximumY, 10))
      self.minimumY = 10**logMinY
      self.maximumY = 10**logMaxY
    if self.options.useLinearScale:
      self.minimumY = 0
      self.maximumY = max(self._getMax(dataHistogram), self._getMax(self.mcTotalHistogram))*1.8
    for mcBackground in self.comparisonInputs.mcBackgrounds:
      self.mcStack.Add(mcBackground.histogram)

    self._createMCTotalGraph()
    self.mcTotalHistogram.GetYaxis().SetRangeUser(self.minimumY, self.maximumY)

    for systematic in self.comparisonInputs.mcBackgrounds.systematics:
      for i in range(0, self.mcTotalHistogram.GetNbinsX()):
        from math import sqrt
        eUP = sqrt(self.mcTotalGraph.GetErrorYhigh(i)**2 + (systematic.histogramUP.GetBinContent(i+1) - self.mcTotalHistogram.GetBinContent(i+1))**2)
        self.mcTotalGraph.SetPointEYhigh(i, eUP)
        if systematic.isSymmetric:
          eDOWN = sqrt(self.mcTotalGraph.GetErrorYlow(i)**2 + (systematic.histogramUP.GetBinContent(i+1) - self.mcTotalHistogram.GetBinContent(i+1))**2)
        else:
          eDOWN = sqrt(self.mcTotalGraph.GetErrorYlow(i)**2 + (systematic.histogramDOWN.GetBinContent(i+1) - self.mcTotalHistogram.GetBinContent(i+1))**2)
        self.mcTotalGraph.SetPointEYlow(i, eDOWN)

    self._makeRatioHistogram()

  def _createMCTotalGraph(self):
    import ROOT
    self.mcTotalGraph = ROOT.TGraphAsymmErrors(self.mcTotalHistogram)
    self.mcTotalGraph.GetXaxis().SetRangeUser(self.mcTotalHistogram.GetBinLowEdge(1), self.mcTotalHistogram.GetBinLowEdge(self.mcTotalHistogram.GetNbinsX()+1))
    self.mcTotalGraph.GetYaxis().SetRangeUser(self.minimumY, self.maximumY)
    self.mcTotalGraph.GetYaxis().SetTitle(self.mcTotalHistogram.GetYaxis().GetTitle())
    self.mcTotalGraph.SetFillColor(ROOT.kGray+3);
    self.mcTotalGraph.SetFillStyle(3004);
    self.mcTotalGraph.SetMarkerSize(0);

  def _printHistogram(self, histogram, openingText=None):
    if openingText is not None:
      print openingText+':'
    else:
      print 'Begin histogram dump:'
    for i in range(1, histogram.GetNbinsX() + 1):
      print '{0}: {1}'.format (i, histogram.GetBinContent(i))
    print

  def _printGraph(self, graph, openingText=None):
    if openingText is not None:
      print openingText+':'
    else:
      print 'Begin graph dump:'
    for i in range(graph.GetN()):
      import ROOT
      x, y = ROOT.Double(0), ROOT.Double(0)
      graph.GetPoint(i, x, y)
      print '{0}: ({1}, {2}) +{3} -{4}'.format (i, x, y, graph.GetErrorYhigh(i), graph.GetErrorYlow(i))
    print

  def _getMin(self, histogram):
    min = None
    for i in range(1, histogram.GetNbinsX()+1):
      val = histogram.GetBinContent(i)
      if val <= 0:
        continue
      if min is None:
        min = val
        continue
      if val < min:
        min = val
    if min is None:
      min = 0.1
    return min

  def _getMax(self, histogram):
    max = None
    for i in range(1, histogram.GetNbinsX()+1):
      val = histogram.GetBinContent(i)
      if val <= 0:
        continue
      if max is None:
        max = val
        continue
      if val > max:
        max = val
    if max is None:
      max = 10.0
    return max

  def _makeHistograms(self):
    if self.dataHistogram is not None: 
      self.dataHistogram.Delete()
    self.dataHistogram = self._refHistogram.Clone(self.name+'Data')
    import ROOT
    self.dataHistogram.SetLineColor(ROOT.kBlack)
    self.dataHistogram.SetFillStyle(0)
    if self.options.plotDataLineWidth is not None:
      self.dataHistogram.SetLineWidth(self.options.plotDataLineWidth)
    if self.options.plotDataMarkerSize is not None:
      self.dataHistogram.SetMarkerSize(self.options.plotDataMarkerSize)

    if self.mcTotalHistogram is not None: 
      self.mcTotalHistogram.Delete()
    self.mcTotalHistogram = self._refHistogram.Clone(self.name+'MCTotal')
    self.mcTotalHistogram.SetLineWidth(self.options.stackLineWidth)
    self.mcTotalHistogram.SetLineColor(ROOT.kBlack)
    self.mcTotalHistogram.SetFillStyle(0)

    if self.mcStack is not None: 
      self.mcStack.Delete()
    self.mcStack = ROOT.THStack(self.name+'MCStack', self.name+'MCStack')

    self.comparisonInputs.mcBackgrounds.makeHistograms(self._refHistogram, self.name)

  def _makeRatioHistogram(self):
    if self.ratioMCHistogram is not None:
      self.ratioMCHistogram.Delete()
    if self.ratioMCGraph is not None:
      self.ratioMCGraph.Delete()
    if self.ratioDataHistogram is not None:
      self.ratioDataHistogram.Delete()
    if self.ratioDataGraph is not None:
      self.ratioDataGraph.Delete()

    if not self.options.blinded:
      self.ratioDataHistogram = self.dataHistogram.Clone(self.name+'ratioData')
    else:
      self.ratioDataHistogram = self.mcTotalHistogram.Clone(self.name+'ratioData')
    self.ratioMCHistogram = self.mcTotalHistogram.Clone(self.name+'ratioMC')

    import ROOT
    self.ratioDataHistogram.SetLineColor(ROOT.kBlack)
    self.ratioDataHistogram.SetLineColor(ROOT.kBlack)
    self.ratioDataHistogram.Divide(self.mcTotalHistogram)
    self.ratioMCHistogram.Divide(self.mcTotalHistogram)

    # If MC total is zero, manually set the ratio to 1!
    for i in range(1, self.mcTotalHistogram.GetNbinsX()+1):
      if self.mcTotalHistogram.GetBinContent(i) == 0:
        self.ratioMCHistogram.SetBinContent(i, 1)

    for i in range(1, self.dataHistogram.GetNbinsX()+1):
      if self.ratioDataHistogram.GetBinContent(i) == 0:
        # Make it so that we can't see them!!
        # FIXME HACKHACKHACKHACKHACKHACKHACKHACKHACKHACKHACK
        self.ratioDataHistogram.SetBinContent(i, 999)

    self.ratioDataGraph = ROOT.TGraphAsymmErrors(self.ratioDataHistogram)
    self.ratioMCGraph = ROOT.TGraphAsymmErrors(self.ratioMCHistogram)

    for i in range(1, self.dataHistogram.GetNbinsX()+1):
      if not self.options.blinded:
        dataBinContent = self.dataHistogram.GetBinContent(i)
        dataBinUncert = self.dataHistogram.GetBinError(i)
      else:
        dataBinContent = self.mcTotalHistogram.GetBinContent(i)
        if dataBinContent <= 0:
          dataBinUncert = 0
        else:
          from math import sqrt
          dataBinUncert = sqrt(dataBinContent)
      mcBinContent = self.mcTotalHistogram.GetBinContent(i)
      mcBinUncertUP = self.mcTotalGraph.GetErrorYhigh(i-1)
      mcBinUncertDOWN = self.mcTotalGraph.GetErrorYlow(i-1)

      if dataBinContent != 0 and mcBinContent != 0:
        ratioDataUncert = dataBinUncert / mcBinContent;
      else:
        ratioDataUncert = 0

      if mcBinContent != 0:
        ratioMCUncertUP = mcBinUncertUP / mcBinContent;
        ratioMCUncertDOWN = mcBinUncertDOWN / mcBinContent;
      else:
        ratioMCUncertUP = 0
        ratioMCUncertDOWN = 0

      self.ratioDataGraph.SetPointEYhigh(i-1, ratioDataUncert)

      self.ratioMCGraph.SetPointEYhigh(i-1, ratioMCUncertUP)
      self.ratioMCGraph.SetPointEYlow(i-1, ratioMCUncertDOWN)

    self._applyRatioFormatting(self.ratioMCGraph)

  def _applyRatioFormatting(self, graph):
    graph.GetXaxis().SetTitleSize(self.options.ratioTitleSizeX)
    if self.options.ratioTitleOffsetX is not None:
      graph.GetXaxis().SetTitleOffset(self.options.ratioTitleOffsetX)
    graph.GetXaxis().SetLabelSize(self.options.ratioLabelSizeX)
    graph.GetXaxis().SetLabelOffset(self.options.ratioLabelOffsetX)
    graph.GetYaxis().SetTitleSize(self.options.ratioTitleSizeY)
    graph.GetYaxis().SetTitleOffset(self.options.ratioTitleOffsetY)
    graph.GetYaxis().SetLabelSize(self.options.ratioLabelSizeY)
    if self.options.ratioLabelOffsetY is not None:
      graph.GetYaxis().SetLabelOffset(self.options.ratioLabelOffsetY)
    graph.GetYaxis().SetNdivisions(404)
    graph.GetYaxis().SetRangeUser(self.options.ratioMin, self.options.ratioMax)
    graph.GetYaxis().SetTitle(self.options.ratioTitle)
    graph.GetXaxis().SetRangeUser(self.dataHistogram.GetBinLowEdge(1), self.dataHistogram.GetBinLowEdge(self.dataHistogram.GetNbinsX()+1))
    graph.GetXaxis().SetTitle(self.dataHistogram.GetXaxis().GetTitle())
    import ROOT
    graph.SetFillColor(ROOT.kGray+3);
    graph.SetFillStyle(3004);
    graph.SetMarkerSize(0);

  def drawStack(self, pad):
    pad.cd()
    pad.SetFillColor(self.options.backgroundColor)
    pad.Modified()
    self.mcStack.Draw('hist')
    self.mcStack.SetMinimum(self.minimumY)
    self.mcStack.SetMaximum(self.maximumY)
    self.mcStack.GetYaxis().SetRangeUser(self.minimumY, self.maximumY)
    self.mcStack.GetYaxis().SetTitle(self.mcTotalHistogram.GetYaxis().GetTitle())
    self.mcStack.GetXaxis().SetLabelOffset(0.1)
    if self.options.stackLabelSize is not None:
      self.mcStack.GetYaxis().SetLabelSize(self.options.stackLabelSize)
    if self.options.stackTitleSize is not None:
      self.mcStack.GetYaxis().SetTitleSize(self.options.stackTitleSize)
    self.mcStack.GetYaxis().SetTitleOffset(self.options.stackTitleOffset)
    pad.Modified()

  def drawRatioHistogram(self, pad):
    pad.cd()
    pad.SetFillColor(self.options.backgroundColor)
    pad.Modified()
    self._applyRatioFormatting(self.ratioMCHistogram)
    self.ratioMCHistogram.Reset()
    self.ratioMCHistogram.Draw()
    self._applyRatioFormatting(self.ratioMCHistogram)
    self.ratioMCGraph.Draw('e2')

    xAxis = self.dataHistogram.GetXaxis()
    minimum = xAxis.GetBinLowEdge(1)
    maximum = xAxis.GetBinUpEdge(xAxis.GetNbins())
    import ROOT
    self.ratioZeroLines = []
    for pos in self.options.ratioLinePositions:
      l = ROOT.TLine(minimum, 1.0, maximum, 1.0)
      self.ratioZeroLines.append(l)
      l.SetLineWidth(self.options.ratioLineWidth);
      l.SetLineColor(self.options.ratioLineColor);
      l.SetLineStyle(self.options.ratioLineStyle);
      if pos == 1.0:
        l.SetLineStyle(self.options.ratioCentralLineStyle);
      l.SetY1(pos)
      l.SetY2(pos)
      l.Draw('same');


    if self.options.suppressXLinesInData:
      for i in range(self.ratioDataGraph.GetN()):
        self.ratioDataGraph.SetPointEXhigh(i, 0)
        self.ratioDataGraph.SetPointEXlow(i, 0)

    if self.options.ratioDataMarkerSize is not None:
      self.ratioDataGraph.SetMarkerSize(self.options.ratioDataMarkerSize)
    self.ratioDataGraph.SetLineWidth(self.options.ratioDataLineWidth)
    self.ratioDataGraph.Draw('pe same')


def makeComparisons(outputDir, comparisons, comparisonInputs, dataWeight=None, extraLumiWeight=None):
  import os
  if not os.path.isdir(outputDir):
    os.makedirs(outputDir)

  import ROOT
  text = ROOT.TLatex()
  text.SetNDC()

  options = comparisons[0].options

  canvas = ROOT.TCanvas('c', 'c', options.canvasWidth, options.canvasHeight)
  plotPad = ROOT.TPad("plotPad", "plotPad", 0.0, 0.3, 1.0, 1.0)
  ratioPad = ROOT.TPad("ratioPad", "ratioPad", 0.0, 0.0, 1.0, 0.3)
  plotPad.SetLogy()
  if options.leftMargin is not None:
    plotPad.SetLeftMargin(options.leftMargin)
    ratioPad.SetLeftMargin(options.leftMargin)
  plotPad.SetRightMargin(options.rightMargin)
  if options.plotPadTopMargin is not None:
    plotPad.SetTopMargin(options.plotPadTopMargin)
  plotPad.SetBottomMargin(options.plotPadBottomMargin)
  ratioPad.SetRightMargin(options.rightMargin)
  ratioPad.SetTopMargin(options.ratioPadTopMargin)
  ratioPad.SetBottomMargin(options.ratioPadBottomMargin)
  plotPad.Draw()
  ratioPad.Draw()

  projections = []
  for comparison in comparisons:
    projections += comparison.getProjections(comparisonInputs, dataWeight, extraLumiWeight)

  from multiprojector import multiProjector
  multiProjector(projections)

  # TODO- might be useful!
  #outFile = ROOT.TFile('test.root', 'RECREATE')
  #for projection in projections:
  #  projection.histogram.SetDirectory(outFile)
  #outFile.Write()
  #for projection in projections:
  #  projection.histogram.SetDirectory(0)
  #outFile.Close()


  for comparison in comparisons:
    opts = comparison.options
    comparison.makeComparison()

    if opts.useLinearScale:
      plotPad.SetLogy(0)
    else:
      plotPad.SetLogy()
    comparison.drawStack(plotPad)
    comparison.mcTotalGraph.Draw('e2 same')
    comparison.mcTotalHistogram.Draw('hist same')
    if not opts.blinded:
      drawString = 'pe same'
      if opts.suppressXLinesInData:
        drawString += 'X0'
      comparison.dataHistogram.Draw(drawString)

    if opts.atlasLabelEnabled:
      text.SetTextSize(opts.atlasLabelTextSize)
      text.SetTextFont(72)
      text.DrawLatex(opts.atlasLabelX, opts.atlasLabelY, 'ATLAS')
      #delx = 0.115*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw());
      delx = 0.081*696*ROOT.gPad.GetWh()/(472*ROOT.gPad.GetWw());
      if opts.atlasLabelText is not None and opts.atlasLabelText:
        text.SetTextFont(42)
        text.SetTextSize(opts.atlasLabelTextSize)
        text.DrawLatex(opts.atlasLabelX+delx, opts.atlasLabelY, opts.atlasLabelText)

    if opts.lumiLabelText is not None:
      text.SetTextFont(42)
      text.SetTextSize(opts.lumiLabelTextSize)
      text.DrawLatex(opts.lumiLabelX, opts.lumiLabelY, opts.lumiLabelText)

    if opts.regionLabelText is not None:
      text.SetTextFont(42)
      text.SetTextSize(opts.regionLabelTextSize)
      text.DrawLatex(opts.regionLabelX, opts.regionLabelY, opts.regionLabelText)

    legend = ROOT.TLegend(opts.legX1, opts.legY1, opts.legX2, opts.legY2)
    legend.SetTextFont(42)
    legend.SetTextSize(opts.legTextSize)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    dataOpt = 'lpf'
    if opts.suppressXLinesInData:
      dataOpt = 'p'
    if opts.legDataMarkerSize is not None:
      marker = ROOT.TMarker()
      marker.SetMarkerStyle(20)
      marker.SetMarkerSize(opts.legDataMarkerSize)
      legend.AddEntry(marker, 'Data', dataOpt)
    else:
      legend.AddEntry(comparison.dataHistogram, 'Data', dataOpt)
    mcTotOpt = 'flp'
    if opts.legMCTotalAsJustFill:
      mcTotOpt = 'f'
    legend.AddEntry(comparison.mcTotalGraph, opts.legMCTotalLabel, mcTotOpt)
    for mcBackground in reversed(comparison.comparisonInputs.mcBackgrounds):
      legend.AddEntry(mcBackground.histogram, mcBackground.label, 'f')
    legend.Draw()

    comparison.drawRatioHistogram(ratioPad)

    comparisonName = '{0}'.format(comparison.name)
    canvas.SaveAs(os.path.join(outputDir, comparisonName+'.pdf'))


def makeTables(outputDir, comparisons, comparisonInputs, bins, dataWeight=None, extraLumiWeight=None):
  import os
  if not os.path.isdir(outputDir):
    os.makedirs(outputDir)

  projections = []
  for comparison in comparisons:
    projections += comparison.getProjections(comparisonInputs, dataWeight, extraLumiWeight)
  from multiprojector import multiProjector
  multiProjector(projections)

  for comparison in comparisons:
    comparison.makeComparison()
    _makeTableForComparison(comparison, bins)
 
def _makeTableForComparison(comparison, bins=range(2,6)):

  if bins is None:
    bins = range(1, comparison.dataHistogram.GetNbinsX()+1)
  #bins = range(2,6)

  text = '\\begin{tabular}{l'+'c'*len(bins)+'}\n\\hline\n'

  maxNameLength = 0
  for mcBackground in comparison.comparisonInputs.mcBackgrounds:
    mcName = _prettyName(mcBackground.name)
    length = len(mcName)
    if length > maxNameLength: maxNameLength = length
  nameWidthStr = str(maxNameLength+2)

  dataHist = comparison.dataHistogram
  dataRow = ('{0:>'+nameWidthStr+'}  ').format('Data')
  for bin in bins:
    dataRow += ' &{0:5.0f}'.format(dataHist.GetBinContent(bin))
  text += dataRow+'\\\\ \n'

  mcTotalHist = comparison.mcTotalHistogram
  graph = comparison.mcTotalGraph
  mcTotalRow = ('{0:>'+nameWidthStr+'}  ').format('Bkg')
  for bin in bins:
    eUP = graph.GetErrorYhigh(bin-1)
    eDN = graph.GetErrorYlow(bin-1)
    eUPPrint = '{0:.2f}'.format(eUP)
    eDNPrint = '{0:.2f}'.format(eDN)
    if eUPPrint == eDNPrint:
      mcTotalRow += ' &${0:5.2f}\pm{1}$'.format(mcTotalHist.GetBinContent(bin), eUPPrint)
    else:
      mcTotalRow += ' &${0:5.2f}^{{+{1}}}_{{-{2}}}$'.format(mcTotalHist.GetBinContent(bin), eUPPrint, eDNPrint)
  text += mcTotalRow+'\\\\\n\hline\n'

  for mcBackground in comparison.comparisonInputs.mcBackgrounds:
    mcName = _prettyName(mcBackground.name)
    row = ('{0:>'+nameWidthStr+'}  ').format(mcName)
    graph = mcBackground.totalUncertaintyGraph()
    mcHist = mcBackground.histogram
    for bin in bins:
      eUP = graph.GetErrorYhigh(bin-1)
      eDN = graph.GetErrorYlow(bin-1)
      eUPPrint = '{0:.2f}'.format(eUP)
      eDNPrint = '{0:.2f}'.format(eDN)
      if eUPPrint == eDNPrint:
        row += ' &${0:5.2f}\pm{1}$'.format(mcHist.GetBinContent(bin), eUPPrint)
      else:
        row += ' &${0:5.2f}^{{+{1}}}_{{-{2}}}$'.format(mcHist.GetBinContent(bin), eUPPrint, eDNPrint)
    text += row + '\\\\\n'
  text += '\hline\n\\end{tabular}'
  
  print text

def _prettyName(name):
  if name == 'higgs': return '$\\ttbar+H$'
  elif name == 'tthiggs': return '$\\ttbar+H$'
  elif name == 'whiggs': return '$W+H$'
  elif name == 'zhiggs': return '$Z+H$'
  elif name == 'topV': return '$\\ttbar+V$'
  elif name == 'diBoson': return '$VV$'
  elif name == 'singleTop': return '$t$'
  elif name == 'singleTopZ': return '$t+Z$'
  elif name == 'zJetsMassiveB': return '$Z+\\mathrm{jets}$'
  elif name == 'topPowheg': return '$\\ttbar$'
  elif name == 'fourTop': return '4 top'
  else: return name.replace('_', '\_')

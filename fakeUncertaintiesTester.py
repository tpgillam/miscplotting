#!/usr/bin/env python

def main():
  treePath = '/r02/atlas/gillam/histFitterRPV/20130302RPV-generalisedMMTest9'

  import ROOT
  ROOT.gROOT.LoadMacro('AtlasStyle.C')
  ROOT.gROOT.LoadMacro('AtlasLabels.C')
  ROOT.SetAtlasStyle()

  import os.path
  fakeEstimateFile = ROOT.TFile(os.path.join(treePath, 'fakeEstimate_skimmed.root'))
  fakeEstimateTree = fakeEstimateFile.Get('fakes_nom')
  
  count = 0
  totalWgt = 0.0
  totalVarWgt = 0.0
  totalWgtWithChargeFlip = 0.0
  totalVarWgtWithChargeFlip = 0.0
  for e in fakeEstimateTree:
    if e.chanDilep == 0 and e.nLooseLep == 2 and e.mll > 90000 and e.mll < 100000:
      totalWgt+= e.fakeLeptWgt
      totalVarWgt+= ((e.fakeLeptWgt_UP-e.fakeLeptWgt)) ** 2
      totalWgtWithChargeFlip += e.fakeLeptWgt * e.chargeFlipWgt
      totalVarWgtWithChargeFlip += (e.chargeFlipWgt * (e.fakeLeptWgt_UP-e.fakeLeptWgt)) ** 2
      #if (count % 10) == 0:
      from math import sqrt
      print totalWgt, sqrt(totalVarWgt), '     ', totalWgtWithChargeFlip, sqrt(totalVarWgtWithChargeFlip)
      count += 1

if __name__ == '__main__':
  main()

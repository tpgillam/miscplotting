#!/usr/bin/env python 
import sys
if '-b' not in sys.argv:
  sys.argv.append('-b')
  import ROOT
  sys.argv = sys.argv[:-1]

def main():
  treePath = '/r02/atlas/gillam/histFitterRPV/20130302RPV-nominal_1'
  outputDir = 'plotsSS_newUncs'

  import ROOT
  ROOT.gROOT.LoadMacro("AtlasStyle.C")
  ROOT.gROOT.LoadMacro("AtlasLabels.C")
  ROOT.SetAtlasStyle()

  import os.path
  backgroundsFile = ROOT.TFile(os.path.join(treePath, 'backgrounds.root'))
  fakeEstimateFile = ROOT.TFile(os.path.join(treePath, 'fakeEstimate.root'))
  chargeFlipFile = ROOT.TFile(os.path.join(treePath, 'chargeFlip.root'))

  from comparator import MCBackground, MCBackgroundList
  backgroundDict = {}
  for systematicPostfix in ['nom', 'JER', 'JESUP', 'JESDOWN', 'RESOST', 'SCALESTUP', 'SCALESTDOWN']:
    mcBackgrounds = MCBackgroundList()
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('topV_'+systematicPostfix), name='topV', label='MadGraph ttbar+V', color=ROOT.kRed-2))
    mcBackgrounds.append(MCBackground(backgroundsFile.Get('diBoson_'+systematicPostfix), name='diBoson', label='Sherpa diBoson', color=ROOT.kRed+3))
    mcBackgrounds.append(MCBackground(fakeEstimateFile.Get('fakes_nom'), name='fakes', label='Fake leptons', color=ROOT.kSpring-7, isDataDriven=True))
    mcBackgrounds.append(MCBackground(chargeFlipFile.Get('chargeFlip_nom'), name='chargeFlip', label='Charge flipped leptons', color=ROOT.kMagenta-2, isDataDriven=True))
    backgroundDict[systematicPostfix] = mcBackgrounds

  egammaPath = os.path.join(treePath, 'egamma.root')
  muonPath = os.path.join(treePath, 'muon.root')
  jettauetmissPath = os.path.join(treePath, 'jettauetmiss.root')

  dataChain = ROOT.TChain('data')
  dataChain.AddFile(egammaPath)
  dataChain.AddFile(muonPath)
  dataChain.AddFile(jettauetmissPath)

  from comparator import ComparisonInputs
  comparisonInputs = ComparisonInputs(dataChain, backgroundDict['nom'])
  nominalWeights = ['mcWgt', 'pileupWgt', 'trigWgt', 'eGammaWgt', 'muonWgt', 'lumiScaling' ,'bTagWgt']
  for weight in nominalWeights:
    comparisonInputs.addWeight(weight)
  #comparisonInputs.addTreeSystematic('JER', backgroundDict['JER'])
  #comparisonInputs.addTreeSystematic('JES', backgroundDict['JESUP'], backgroundDict['JESDOWN'])
  #comparisonInputs.addTreeSystematic('RESOST', backgroundDict['RESOST'])
  #comparisonInputs.addTreeSystematic('SCALEST', backgroundDict['SCALESTUP'], backgroundDict['SCALESTDOWN'])
  #comparisonInputs.addWeightSystematic('BJET', '*(bTagWgt_BJETUP/bTagWgt)', '*(bTagWgt_BJETDOWN/bTagWgt)')
  #comparisonInputs.addWeightSystematic('MISTAG', '*(bTagWgt_MISTAGUP/bTagWgt)', '*(bTagWgt_MISTAGDOWN/bTagWgt)')
  #comparisonInputs.addWeightSystematic('EGAMMA', '*(eGammaWgt_UP/eGammaWgt)', '*(eGammaWgt_DOWN/eGammaWgt)')
  #comparisonInputs.addWeightSystematic('MUON', '*(muonWgt_UP/muonWgt)', '*(muonWgt_DOWN/muonWgt)')

  comparisonInputs.addWeightWithSystematic('CHARGEFLIP', 'chargeFlipWgt', 'chargeFlipWgt_UP', mcBackgroundNamesToUse=['chargeFlip', 'fakes'])
  comparisonInputs.addWeightWithSystematic('FAKEEL', 'fakeLeptWgt', 'fakeLeptWgt_El_UP', mcBackgroundNamesToUse='fakes')
  comparisonInputs.addWeightWithSystematic('FAKEMU', 'fakeLeptWgt', 'fakeLeptWgt_Mu_UP', mcBackgroundNamesToUse='fakes')

  from comparator import Comparison
  comparisons = []
  for nBJets in [0,1,2,3]:
    for lepChan in [None, 0, 1, 2]:
      name = '-'+str(nBJets)+'b'
      if lepChan is not None:
        if lepChan == 0:
          name += '-ee'
        elif lepChan == 1:
          name += '-em'
        elif lepChan == 2:
          name += '-mm'

      if nBJets > 0:
        weight = '(nBJets20>={0})'.format(nBJets)
      else:
        weight = '1'
      if lepChan is not None:
        weight += ' * (chanDilep=={0})'.format(lepChan)
      comparisons.append(Comparison('mll/1000', ROOT.TH1D('mll'+name, ';m_{ll} [GeV];Events / 60 GeV', 10, 0, 600), weight))
      comparisons.append(Comparison('met/1000', ROOT.TH1D('met'+name, ';E^{T}_{miss} [GeV];Events / 50 GeV', 10, 0, 500), weight))
      comparisons.append(Comparison('meff/1000', ROOT.TH1D('meff'+name, ';m_{eff} [GeV];Events / 100 GeV', 15, 0, 1500), weight))
      comparisons.append(Comparison('nJets20', ROOT.TH1D('nJets20'+name, ';nJets > 20 GeV;Events', 12, 0, 12), weight))
      comparisons.append(Comparison('nBJets20', ROOT.TH1D('nBJets20'+name, ';nBJets > 20 GeV;Events', 7, 0, 7), weight))
      if nBJets >= 1:
        comparisons.append(Comparison('bJet1Pt/1000', ROOT.TH1D('bJet1Pt'+name, ';1st hardest b-jet p_{T} [GeV];Events / 50 GeV', 10, 0, 500), weight))
      if nBJets >= 2:
        comparisons.append(Comparison('bJet2Pt/1000', ROOT.TH1D('bJet2Pt'+name, ';2nd hardest b-jet p_{T} [GeV];Events / 50 GeV', 10, 0, 500), weight))
      if nBJets >= 3:
        comparisons.append(Comparison('bJet3Pt/1000', ROOT.TH1D('bJet3Pt'+name, ';3rd hardest b-jet p_{T} [GeV];Events / 40 GeV', 10, 0, 400), weight))

  from comparator import makeComparisons
  makeComparisons(outputDir, comparisons, comparisonInputs, None, 20661.0/20694.9)

  backgroundsFile.Close()
  fakeEstimateFile.Close()


if __name__ == '__main__':
  main()

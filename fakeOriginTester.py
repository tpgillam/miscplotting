#!/usr/bin/env python

def main():
  treePath = '/r02/atlas/gillam/histFitterRPV/20130302RPV-generalisedMMTest7'

  import ROOT
  ROOT.gROOT.LoadMacro('AtlasStyle.C')
  ROOT.gROOT.LoadMacro('AtlasLabels.C')
  ROOT.SetAtlasStyle()

  import os.path
  fakeEstimateFile = ROOT.TFile(os.path.join(treePath, 'fakeEstimate.root'))
  fakeEstimateTree = fakeEstimateFile.Get('fakes_nom')

  fakeOrigin = ROOT.TH1F('fakeOrigin', 'fakeOrigin;Input config;Events', 9, 0, 9)
  fakeOriginWeighted = ROOT.TH1F('fakeOriginWeighted', 'fakeOriginWeighted;Input config;Weight' , 9, 0, 9)
  fakeFlavourWeighted = ROOT.TH1F('fakeFlavourWeighted', 'fakeFlavourWeighted;Input flavour;Weight' , 9, 0, 9)
  fakeOriginMll = ROOT.TH1F('fakeOriginMll', 'fakeOriginMll;m_{ll} [GeV];Events / 60 GeV', 10, 0, 600)
  addLabels(fakeOrigin)
  addLabels(fakeOriginWeighted)
  addFlavourLabels(fakeFlavourWeighted)

  #weight = 'mcWgt * pileupWgt * trigWgt * eGammaWgt * muonWgt * lumiScaling * bTagWgt * fakeLeptWgt * chargeFlipWgt *' 
  weight = 'mcWgt * pileupWgt * trigWgt * eGammaWgt * muonWgt * lumiScaling * bTagWgt * fakeLeptWgt * (isElBitmask == 1) *' 
  from multiprojector import Projection, multiProjector
  projections = []
  projections.append(Projection(fakeEstimateTree, fakeOrigin, 'origTLBitmask', '(nLep15 == 3) * (isElBitmask == 1)'))
  projections.append(Projection(fakeEstimateTree, fakeOrigin, '8.5', '(nLep15 == 3) * (nLooseLep > 3) * (isElBitmask == 1)'))
  projections.append(Projection(fakeEstimateTree, fakeOriginWeighted, 'origTLBitmask', weight+'(nLooseLep == 3) * (TLBitmask == 7)'))
  projections.append(Projection(fakeEstimateTree, fakeOriginWeighted, '8.5', weight+'(nLep15 == 3) * (nLooseLep > 3)'))
  projections.append(Projection(fakeEstimateTree, fakeFlavourWeighted, 'isElBitmask', weight+'(nLooseLep == 3) * (TLBitmask == 7)'))
  projections.append(Projection(fakeEstimateTree, fakeFlavourWeighted, '8.5', weight+'(nLep15 == 3) * (nLooseLep > 3)'))
  projections.append(Projection(fakeEstimateTree, fakeOriginMll, 'mll/1000', weight+'(nLep15 == 3)'))
  multiProjector(projections)

  xAxis = fakeOriginWeighted.GetXaxis()
  minimum = xAxis.GetBinLowEdge(1)
  maximum = xAxis.GetBinUpEdge(xAxis.GetNbins())
  ratioZeroLine = ROOT.TLine(minimum, 0.0, maximum, 0.0)
  ratioZeroLine.SetLineWidth(2);
  ratioZeroLine.SetLineColor(ROOT.kRed);

  c = ROOT.TCanvas()
  c.cd()
  fakeOrigin.Draw()
  c.SaveAs('fakeOrigin.pdf') 

  fakeOriginWeighted.Draw()
  ratioZeroLine.Draw('same');
  c.SaveAs('fakeOriginWeighted.pdf') 

  fakeFlavourWeighted.Draw()
  ratioZeroLine.Draw('same');
  c.SaveAs('fakeFlavourWeighted.pdf') 

  fakeOriginMll.Draw()
  c.SaveAs('fakeOriginMll.pdf')


def addLabels(histogram):
  for tlConfig in threeLeptonTLConfigs():
    label = tlConfig.asString()
    binNumber = 1 + tlConfig.asBitmask()
    histogram.GetXaxis().SetBinLabel(binNumber, label)
  histogram.GetXaxis().SetBinLabel(9, '>3 lep')

def addFlavourLabels(histogram):
  for tlConfig in threeLeptonTLConfigs():
    label = tlConfig.asString()
    label = label.replace('T', 'E')
    label = label.replace('L', 'M')
    binNumber = 1 + tlConfig.asBitmask()
    histogram.GetXaxis().SetBinLabel(binNumber, label)
  histogram.GetXaxis().SetBinLabel(9, '>3 lep')

class TLConfig(object):
  def __init__(self, leptonTightList=None):
    if leptonTightList == None:
      leptonTightList = []
    self._leptonTightList = leptonTightList

  def __eq__(self, other):
    numLeptonsInSelf = len(self._leptonTightList)
    numLeptonsInOther = len(other._leptonTightList)
    if numLeptonsInSelf != numLeptonsInOther:
      return False
    for i in range(numLeptonsInSelf):
      if self._leptonTightList[i] != other._leptonTightList[i]:
        return False
    return True

  def asBitmask(self):
    result = 0
    leptonIndex = 0
    for isTight in self._leptonTightList:
      if isTight:
        result += 1 << leptonIndex
      leptonIndex += 1
    return result

  def asString(self):
    result = ''
    for isTight in self._leptonTightList:
      if isTight:
        result += 'T'
      else:
        result += 'L'
    return result

def threeLeptonTLConfigs():
  configLists = [[0,0,0], [1,0,0], [0,1,0], [0,0,1], [1,1,0], [1,0,1], [0,1,1], [1,1,1]]
  return tlConfigsFromLeptonTightLists(configLists)

def tlConfigsFromLeptonTightLists(leptonTightLists):
  result = []
  for leptonTightList in leptonTightLists:
    result.append(TLConfig(leptonTightList))
  return result

if __name__ == '__main__':
  main()

class Branch(object):
  def __init__(self, name, tree):
    self.name = name
    self._tree = tree
    self.typeString = self._getTypeString()

  def _getTypeString(self):
    for x in self._tree.GetListOfBranches():
      if x.GetName() == self.name:
        # TODO Does it matter if more than one leaf per branch?
        for l in x.GetListOfLeaves():
          return l.GetTypeName()

  def __repr__(self):
    return '{0} {1}'.format(self.typeString, self.name)


def _getBranches(projections):
  if len(projections) < 1: return []
  tree = projections[0].tree
  candidateBranchNames = [x.GetName() for x in tree.GetListOfBranches()]
  branchNames = set()
  for projection in projections:
    branchNames |= _branchNamesInString(projection.variable, candidateBranchNames)
    branchNames |= _branchNamesInString(projection.cuts, candidateBranchNames)
  branches = []
  for branchName in branchNames:
    branches.append(Branch(branchName, tree))
  return branches

def _branchNamesInString(testString, candidates):
  branchNames = set()
  for candidate in candidates:
    # FIXME This will add too many branches!!
    if candidate in testString:
      branchNames.add(candidate)
  return branchNames

def _makeMacroString(projectionsByTree):
  import ROOT
  mainFunction = 'void fillHistograms() { '
  macro = '#include <TTree.h>\n'
  macro += '#include <TH1.h>\n'
  macro += '#include <iostream>\n'
  count = 0
  for tree in projectionsByTree:
    projections = projectionsByTree[tree]
    if len(projections) < 1: continue
    macro += _makeTreeProcessingFunction(projections, str(count))
    mainFunction += 'treeProcessing{0}(); '.format(str(count))
    count += 1

  mainFunction += ' }\n'
  macro += mainFunction
  macro += 'int main() { fillHistograms(); return 0; }'
  return macro

def _makeTreeProcessingFunction(projections, nameTag):
  import ROOT
  tree = projections[0].tree
  branches = _getBranches(projections)
  weightGroupedProjections = _weightGroupedProjections(projections)

  macro = 'void treeProcessing{0}() {{\n'.format(nameTag)
  macro += 'std::cout << "Processing {0}" << std::endl;'.format(tree.GetName())
  macro += 'TTree *_tree = (TTree *) {0};\n'.format(ROOT.AddressOf(tree)[0])

  definedHistograms = set()
  for projection in projections:
    hist = projection.histogram
    if not hist in definedHistograms:
      histName = projection.sanitisedHistogramName()
      macro += '{0} *_hist_{2} = ({0} *) {1};\n'.format(hist.Class().GetName(), ROOT.AddressOf(hist)[0], histName)
      macro += 'Double_t _position_{0};\n'.format(histName)
      macro += '\n'
      definedHistograms.add(hist)

  for weightObject in weightGroupedProjections:
    weightName = weightObject[0]
    macro += 'Double_t {0};\n'.format(weightName)
  macro += '\n'

  macro += '_tree->SetBranchStatus("*", 0);\n'
  for branch in branches:
    macro += '{0} {1};\n'.format(branch.typeString, branch.name)
    macro += '_tree->SetBranchStatus("{0}", 1);\n'.format(branch.name)
    macro += '_tree->SetBranchAddress("{0}", &{0});\n'.format(branch.name)
    macro += '\n'

  macro += 'for (Long64_t i = 0; i < _tree->GetEntries(); ++i) {\n'
  macro += '  _tree->GetEntry(i);\n'
  for weightObject in weightGroupedProjections:
    weightName = weightObject[0]
    weight= weightObject[1]
    macro += '  {0} = ({1});\n'.format(weightName, weight)
    macro += '  if ({0} != 0.f) {{\n'.format(weightName)
    for projection in weightGroupedProjections[weightObject]:
      histName = projection.sanitisedHistogramName()
      macro += '    _position_{0} = ({1});\n'.format(histName, projection.variable)
      macro += '    _hist_{0}->Fill(_position_{0}, {1});\n'.format(histName, weightName)
    macro += '  }\n'
    macro += '\n'
  macro += '}\n'
  macro += '}\n'
  return macro

def _weightGroupedProjections(projections):
  projectionsByCommonWeight = _projectionsByCommonWeight(projections)
  result = {}
  count = 0
  for weight in projectionsByCommonWeight:
    result[('_weight_{0}'.format(count), weight)] = projectionsByCommonWeight[weight]
    count += 1
  return result


def _projectionsByCommonWeight(projections):
  result = {}
  for projection in projections:
    if projection.cuts not in result:
      result[projection.cuts] = []
    result[projection.cuts].append(projection)
  return result
  
def _projectAll(projectionsByTree):
  macroString = _makeMacroString(projectionsByTree)

  import os
  from tempfile import mkdtemp
  tempDir = mkdtemp()
  macroFilename = os.path.join(tempDir, 'fillHistograms.cxx')
  soFilename = os.path.join(tempDir, 'fillHistograms_cxx.so')
  print 'Creating macro:', macroFilename
  f = open(macroFilename, 'w')
  f.write(macroString)
  f.close()
  #f = open('test.cxx', 'w')
  #f.write(macroString)
  #f.close()

  import ROOT
  print 'Compiling macro'
  import time
  startTime = time.time()
  ROOT.gSystem.CompileMacro(macroFilename, 'fO')
  print 'Compiled in {0} s'.format(time.time() - startTime)
  ROOT.gROOT.ProcessLine('fillHistograms()')
  ROOT.gSystem.Unload(soFilename)
  import shutil
  shutil.rmtree(tempDir)



class Projection(object):
  def __init__(self, tree, histogram, variable, cuts):
    self.tree = tree
    self.histogram = histogram
    self.variable = variable
    self.cuts = cuts

  def sanitisedHistogramName(self):
    name = self.histogram.GetName()
    name = name.replace('-', '_')
    #TODO expand replacement list for bad characters
    return name

  def __repr__(self):
    return '{0}, {1}, varexp={2}, cuts={3}'.format(self.tree, self.histogram, self.variable, self.cuts)


def multiProjector(projections):
  if isinstance(projections, Projection):
    projections = [projections,]
  projectionsByTree = {}
  for projection in projections:
    tree = projection.tree
    if tree not in projectionsByTree:
      print 'Adding tree',tree
      projectionsByTree[tree] = []
    projectionsByTree[tree].append(projection)
  _projectAll(projectionsByTree)


def test():
  import ROOT
  f = ROOT.TFile('/r02/atlas/gillam/histFitterRPV/20130302RPV-OS2b_1/backgrounds.root')
  t = f.Get('topV_nom')
  t2 = f.Get('topV_JESUP')
  h1 = ROOT.TH1D('h1', 'h1', 100, 0, 300)
  h2 = ROOT.TH1D('h2', 'h2', 100, 0, 300)
  projections = []
  projections.append(Projection(t, h1, 'lep1Pt*met/100000000', '(mll > 20000) * (chanDilep==0)'))
  projections.append(Projection(t2, h2, 'lep2Pt/1000', 'mll > 20000'))
  multiProjector(projections)

  c = ROOT.TCanvas('c')
  c.cd()
  h1.Draw()
  from time import sleep
  sleep(3)
  h2.Draw()
  c.Update()
  sleep(3)

  f.Close()

if __name__ == '__main__':
  test()
